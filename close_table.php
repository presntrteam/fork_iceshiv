<?php
	$filename = "Item wise result";
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");
	
	if(isset($_POST['submit']))
	{
		$tablestatus = $_POST['tablestatus'];	
		$date = $_POST['date'];
	}
	else
	{
		$tablestatus = "";	
		$date = date('d-m-Y');
	}
	
	
?>
	<div class="content-wrapper">
		<section class="content-header">
        	<h1>
            	<?=$filename?>
            	<small>it all starts here</small>
            </h1>
         	<ol class="breadcrumb">
            	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            	<li class="active"><?=$filename?></li>
          	</ol>
        </section>
        <section class="content">
        	<div class="box box-primary">
                <div class="box-header with-border">
	            	<h3 class="box-title">Filter by date</h3>
	            	<div class="box-tools pull-right"></div>
            	</div>
                <div class="row">
                	<div class="col-md-6">
                        <form role="form" class="form-horizontal" name="frm_additem" action="" id="frm_additem" method="post" enctype="multipart/form-data" >
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">Select filter</label>
                                   	<div class="col-sm-9">
                                        <select id="tablestatus" name="tablestatus">
                                            <option <?php echo ($tablestatus == "") ? 'selected' : ''; ?> value="">All  Table</option>
                                            <option value="L" <?php echo ($tablestatus == "L") ? 'selected' : ''; ?>>Live Table</option>
                                            <option value="C" <?php echo ($tablestatus == "C") ? 'selected' : ''; ?>>Close Table</option>
                                            <option value="R" <?php echo ($tablestatus == "R") ? 'selected' : ''; ?>> Revice Table</option>
                                        </select>
                                    </div>
                                </div>	
                                <div class="form-group">	
                                    <label class="col-sm-3 control-label" for="inputEmail3">Select Date</label>
                                    <div class="col-sm-9">
                                        <input id="datepicker" value="<?php echo $date ?>" class="form-control pull-right" type="text" placeholder="Select Date" name="date" required="">
                                    </div>
                                </div>  
                            </div>  
                            
                            <div class="box-footer col-md-9" align="center">
                                <button class="btn btn-primary" type="submit" name="submit">Go</button>
                            </div>
                        </form>              
                    </div> 
                </div>
                <hr style="border-width: 5px;">
                <div class="row">
                    <div class="col-md-11 center-block float-none">
                        <div class="portlet light bordered">
                            <div class="portlet-title"><div class="caption font-dark"><span class="caption-subject bold uppercase">&nbsp;</span></div></div>
                            <div class="portlet-body">
                                <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                                    <div class="table-scrollable">
                                        <table border="1" id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column no-footer" role="grid" aria-describedby="sample_1_info">
                                            <thead>
                                                <tr role="row" align="center" class="text_center">
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > <input type="checkbox" name="checkall" value="1" id="checkall"/> </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Payment </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Print </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill number </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Table </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Waiter </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Table Part </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Amount </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Qty </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Start Time </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Last Order Time </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > View </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                            $joinItemOrderedItems = "SELECT bm.billid,bm.isvisible, bm.t_status, bm.bill_table, wm.waiter, bm.table_part, bm.bill_time, bm.update_time, sum(bim.bim_item_quantity) as qty, sum(bim.bim_item_price) as amt FROM `bill_master` as bm LEFT JOIN waiter_master AS wm ON wm.waiter_id=bm.bill_waiter LEFT JOIN bill_item_master AS bim ON bm.billid=bim.billid WHERE date_format(bill_date, '%d-%m-%Y') = '".$date."'"; 
															if($tablestatus != "")
																$joinItemOrderedItems .= " AND bm.t_status = '".$tablestatus."'";	
															$joinItemOrderedItems .= " GROUP BY billid";
															
                                                            $joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
                                                            
															
															$t_qty = 0;
															$t_amt = 0;
															while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
                                                                if($joinItemOrderedItemsRow['isvisible'] == '1'){
																?>
                                                                    <tr class="gradeX odd rm<?php echo $joinItemOrderedItemsRow['billid']; ?>" role="row">
                                                                        <td align="center"><input type="checkbox" name="billidchk[]" class="billidchk" value="<?php echo $joinItemOrderedItemsRow['billid']; ?>" /></td>
                                                                        <td align="center"> 
                                                                        	<a href="receive.php?orderId=<?php echo $joinItemOrderedItemsRow['billid']; ?>">
																			<?php 
																				if($joinItemOrderedItemsRow['t_status'] == "L") 
																					echo "Live Table";
																				elseif($joinItemOrderedItemsRow['t_status'] == "C")
																					echo "Close Table";
																				else
																					echo "Receive Table";	
																			?> 
                                                                        	</a>
                                                                        </td>
                                                                        <td align="center"> Print </td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['billid']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bill_table']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['waiter']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['table_part']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['amt']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['qty']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bill_time']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['update_time']; ?></td> 
                                                                        <td align="center"><a class="vierOrder btn-default btn" data-url="<?php echo $ROOT_SITE_URL ?>viewTable.php?orderId=<?php echo $joinItemOrderedItemsRow['billid']; ?>">View</a> | <a class="vierOrder btn-default btn" data-url="<?php echo $ROOT_SITE_URL ?>printName.php?orderId=<?php echo $joinItemOrderedItemsRow['billid']; ?>">Print With Name</a>   </td> 
                                                                    </tr>
                                                                <?php
																$t_qty = $t_qty+$joinItemOrderedItemsRow['qty'];
																$t_amt = $t_amt+$joinItemOrderedItemsRow['amt'];
																}
													        }
                                                  ?>  
                                            </tbody>
											<tfoot>
                                                  <tr class="gradeX odd" role="row">
                                                        <td align="right"></td>
                                                        <td align="right"></td>
                                                        <td align="right"></td>
                                                        <td align="right"></td>
                                                        <td align="right"><a class="btn btn-primary deletebill" href="#">Delete  Checked</a></td>
                                                        <td align="center"><a class="btn btn-default clearall" href="#">Clear All</a></td>
                                                        <td align="right">Total</td>
                                                        <td align="right"><?php echo $t_amt; ?></td>
                                                        <td align="right"><?php echo $t_qty; ?></td>
                                                        <td align="right"></td>
                                                        <td align="right"></td>
                                                        <td align="right"></td>
                                                    </tr>   
											</tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
        	</div>
        </section>
    </div>      
<script>
	jQuery(document).ready(function() {
		
		$(".vierOrder").click(function(){
			window.open( $(this).attr('data-url'), "Shiv view order","status = 1, height = 800, width = 700, resizable = 0" );
			
		})
		
		$('#datepicker').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy',
			todayHighlight: true
		});
				
		$('#datepicker1').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy',
			todayHighlight: true
		});
		
		$(".clearall").click(function(){
			var co = confirm('Are you sure?');
			if (co == true) {
					var allbillids="";
					$(".billidchk").each(function(){
							if(allbillids != "")
								allbillids = allbillids+',';
							allbillids = allbillids+$(this).val();
							$(".rm"+$(this).val()).remove();
						
					})
					$.ajax({
						type: "POST",
						url: "<?php echo 'ajax_delete_bill_table.php' ?>",
						data: {allbillids:allbillids},
						cache: false,
							success: function(html) {
								
							}	
					});
					window.location.href='close_table.php';
					return false;
			}
			else
			{
				return false;
			}
		})
		
		$(".deletebill").click(function(){
			var co = confirm('Are you sure?');
			if (co == true) {
					var allbillids="";
					$(".billidchk").each(function(){
						if ($(this).prop('checked')) {
							if(allbillids != "")
								allbillids = allbillids+',';
							allbillids = allbillids+$(this).val();
							$(".rm"+$(this).val()).remove();
						}
						
					})
					$.ajax({
						type: "POST",
						url: "<?php echo 'ajax_delete_bill_table.php' ?>",
						data: {allbillids:allbillids},
						cache: false,
							success: function(html) {
								
							}	
					});
					return false;
			}
			else
			{
				return false;
			}
		});		
		
		
		$('.dataTable').DataTable ({
			"order": [[ 2, "desc" ]]
		});
		
		$("#checkall").change(function(){
			if ($(this).prop('checked')) {
				$(".billidchk").prop( "checked", true );
			}
			else
			{
				$(".billidchk").prop( "checked", false );
			}
		})
		
    });
</script>
<?php
	include_once("includes/footer.php");
?>
      
