<?php
	$filename = "Export Bills";
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");
	
	
	if(isset($_POST['submit']))
	{
		$fdate = $_POST['date1'];	
		$tdate = $_POST['date'];
	}
	
		$tablestatus = "";	
		$date = date('d-m-Y');
	
		
	//$date = date('d-m-Y');	
?>
	<div class="content-wrapper">
		<section class="content-header">
        	<h1>
            	<?=$filename?>
            	<small>it all starts here</small>
            </h1>
			
         	<ol class="breadcrumb">
            	<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            	<li class="active"><?=$filename?></li>
          	</ol>
        </section>
		
        <section class="content">
		
        	<div class="box box-primary">
			
			<div class="box-header with-border">
	            	<h3 class="box-title">Filter by date</h3>
	            	<div class="box-tools pull-right"></div>
            	</div>
                <div class="row">
                	<div class="col-md-6">
                        <form role="form" class="form-horizontal" name="frm_additem" action="" id="frm_additem" method="post" enctype="multipart/form-data" >
                            <div class="box-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">Start Date</label>
                                    <div class="col-sm-9">
                                        <input id="datepicker1" value="<?php echo $fdate; ?>" class="form-control pull-right" type="text" placeholder="Select Date" name="date1" required="">
                                    </div>
                                </div>	
                                <div class="form-group">	
                                    <label class="col-sm-3 control-label" for="inputEmail3">End Date</label>
                                    <div class="col-sm-9">
                                        <input id="datepicker" value="<?php echo $tdate; ?>" class="form-control pull-right" type="text" placeholder="Select Date" name="date" required="">
                                    </div>
                                </div>  
                            </div>  
                            
                            <div class="box-footer col-md-9" align="center">
                                <button class="btn btn-primary" type="submit" name="submit">Go</button>
                            </div>
                        </form>              
                    </div> 
                </div>
	
			
                <div class="row">
                    <div class="col-md-9 center-block float-none">
                        <div class="portlet light bordered">
                            <div class="portlet-title"><div class="caption font-dark"><span class="caption-subject bold uppercase">&nbsp;</span></div></div>
                            <div class="portlet-body">
                                <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
                                    <div class="table-scrollable">
                                        <table border="1" id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" role="grid" aria-describedby="sample_1_info">
                                            <thead>
                                                <tr role="row" align="center" class="text_center">
                                               		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > No. </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill number </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Table </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Waiter </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Table Part </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Amount </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Qty </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Start Time </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Last Order Time </th>
                                                    <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Date </th>
                                                </tr>
                                                <?php 
                                                            $joinItemOrderedItems = "SELECT bm.billid,bm.bill_date,bm.isvisible, bm.t_status, bm.bill_table, wm.waiter, bm.table_part, bm.bill_time, bm.update_time, sum(bim.bim_item_quantity) as qty, sum(bim.bim_item_price) as amt FROM `bill_master` as bm LEFT JOIN waiter_master AS wm ON wm.waiter_id=bm.bill_waiter LEFT JOIN bill_item_master AS bim ON bm.billid=bim.billid WHERE bm.isvisible = '1'"; 
															$joinItemOrderedItems .= " AND bm.t_status = 'C'";
															if(isset($_POST['submit']))
															{			
																$joinItemOrderedItems .= " AND date_format(bm.bill_date, '%Y-%m-%d') between '".date_format(date_create($fdate),'Y-m-d')."' AND '".date_format(date_create($tdate),'Y-m-d')."'";
																			
															}else{
																$joinItemOrderedItems .= " AND date_format(bill_date, '%d-%m-%Y') = '".$date."'";
															}
															$joinItemOrderedItems .= " GROUP BY billid";
															$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
                                                            $t_qty = 0;
															$t_amt = 0;
															$co = 1;
															while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
                                                                ?>
                                                                    <tr class="gradeX odd rm<?php echo $joinItemOrderedItemsRow['billid']; ?>" role="row">
                                                                    	<td align="right"><?php echo $co; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['billid']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bill_table']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['waiter']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['table_part']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['amt']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['qty']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['bill_time']; ?></td>
                                                                        <td align="right"><?php echo $joinItemOrderedItemsRow['update_time']; ?></td> 
                                                                        <td align="right"><?php echo date_format(date_create($joinItemOrderedItemsRow['bill_date']),"d/m/Y"); ?></td> 
                                                                    </tr>
                                                                <?php
																$t_qty = $t_qty+$joinItemOrderedItemsRow['qty'];
																$t_amt = $t_amt+$joinItemOrderedItemsRow['amt'];
																$co++;
													        }
                                                  ?>  
                                                <tr class="gradeX odd" role="row">
                                                    <td colspan="4" align="center"><a class="btn btn-primary" id="exportbill" href="#">Export Bills</a></td>
                                                    <td align="right">Total</td>
                                                    <td align="right"><?php echo $t_amt; ?></td>
                                                    <td align="right"><?php echo $t_qty; ?></td>
													<td align="right"></td>
                                                </tr>     
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                </div>
        	</div>
        </section>
    </div>      
<script>
	jQuery(document).ready(function() {
		
		$("#exportbill").click(function(){
			var fromDate = $("#datepicker1").val();
			var toDate = $("#datepicker").val();
			//alert(fromDate);
			//alert(toDate);
			
			window.location.href='menu/excel/index.php?fromdate='+fromDate+' & todate='+toDate;
			
		})
		
		$(".vierOrder").click(function(){
			window.open( $(this).attr('data-url'), "Shiv view order","status = 1, height = 800, width = 700, resizable = 0" );
			
		})
		
		$('#datepicker').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy',
			todayHighlight: true
		});
				
		$('#datepicker1').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy',
			todayHighlight: true
		});
		
		$(".clearall").click(function(){
			var co = confirm('Are you sure?');
			if (co == true) {
					var allbillids="";
					$(".billidchk").each(function(){
							if(allbillids != "")
								allbillids = allbillids+',';
							allbillids = allbillids+$(this).val();
							$(".rm"+$(this).val()).remove();
						
					})
					$.ajax({
						type: "POST",
						url: "<?php echo 'ajax_delete_bill_table.php' ?>",
						data: {allbillids:allbillids},
						cache: false,
							success: function(html) {
								
							}	
					});
					return false;
			}
			else
			{
				return false;
			}
		})
		
		$(".deletebill").click(function(){
			var co = confirm('Are you sure?');
			if (co == true) {
					var allbillids="";
					$(".billidchk").each(function(){
						if ($(this).prop('checked')) {
							if(allbillids != "")
								allbillids = allbillids+',';
							allbillids = allbillids+$(this).val();
							$(".rm"+$(this).val()).remove();
						}
						
					})
					$.ajax({
						type: "POST",
						url: "<?php echo 'ajax_delete_bill_table.php' ?>",
						data: {allbillids:allbillids},
						cache: false,
							success: function(html) {
								
							}	
					});
					return false;
			}
			else
			{
				return false;
			}
		});		
		
		$("#checkall").change(function(){
			if ($(this).prop('checked')) {
				$(".billidchk").prop( "checked", true );
			}
			else
			{
				$(".billidchk").prop( "checked", false );
			}
		})
		
    });
</script>
<?php
	include_once("includes/footer.php");
?>
      
