<?php
	$filename = "Bills Deduction";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");
	
		$sel_qry = "SELECT count(*) as cnt FROM bill_master";
		if ($ins_qry_res = mysql_query($sel_qry))
		{
			$fetch_rec = mysql_fetch_assoc($ins_qry_res);
			$num_rows = $fetch_rec["cnt"];
		}
		
	$itemsPerPage = 20;
	if(isset($_GET['page']) && $_GET['page'] != ""){
		$currentPage = $_GET['page'];
		
	}else{
		
		$currentPage = '0';
	}
	$totalItems = $num_rows;
	if($currentPage >0 ){
		$startAt = $itemsPerPage * ($currentPage - 1);
	}else{
		$startAt = 0;
	}	
	$cnt = $startAt;
	$urlPattern = 'list_bill.php?page=(:num)';
	
	$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
	
	
	$sel_data_qry = "SELECT * FROM 	bill_master  limit $startAt, $itemsPerPage";
	if ($ins_qry_data_res = mysql_query($sel_data_qry))
	{ 	// echo "New record created successfully";
		$num_rows = mysql_num_rows($ins_qry_data_res);
		
	}else{  echo "Error: " . $sel_qry . "<br>" . mysql_error($db);  }

?>
  

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?=$filename?>
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?=$filename?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Bills Deduction</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
          <!-- /.box-body -->
            
			
			
			<!--Add_Item_ST-->
			<div class="col-md-12" style="margin:25px 0px;">

			
			<?php
				if(isset($_REQUEST['final_left']) &&  $_REQUEST['final_left'] != ""){
				?>
					<div class="bg-green color-palette">
						<span style="padding:15px; line-height:40px; font-weight:600;">
								Total decution done <?php echo $_REQUEST['original_amount'] - $_REQUEST['final_left']; ?> From <?php echo $_REQUEST['original_amount']; ?>
					     </span>
					</div>
					<br/><br/>
				<?php
				}else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "deductionnotpossible"){ ?>
						<div class="bg-green color-palette">
						<span style="padding:15px; line-height:40px; font-weight:600;">
								No more dectuction possible! Please check Final total.
					     </span>
					</div>
					<br/><br/>
			<?php
				}
			?>
			
			<form role="form" class="form-horizontal"  action="deduction_db.php" name="frm_additem" id="frm_additem" method="post" >
			
			<input type="hidden" name="myaction" id="myaction"  value="additem" />
			<input type="hidden" name="itemid" id="itemid"  value="" />
			
			<div class="box-body">
		
			
			<label class="col-sm-3 control-label" for="inputEmail3">Add Deduction Amount </label>
			<div class="col-sm-3">
				<input type="text" class="form-control" name="deduction_amount" id="deduction_amount"  />
			</div>
		
		
			<button class="btn btn-primary" type="submit">Submit</button>
			</div>
			</form>
			<div class="clear">&nbsp;&nbsp;</div>
			</div>
			<!--Add_Item_EN-->
			
			
			<div class="box-footer">
              
			<!--DATA_TABLE_ST-->
					<div class="row">
						<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet light bordered">
						<div class="portlet-title">
						<div class="caption font-dark"> <span class="caption-subject bold uppercase">&nbsp;</span> </div>
						</div>
						<div class="portlet-body">
						<div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
						
						<div class="table-scrollable">
						<table id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" role="grid" aria-describedby="sample_1_info">
						<thead>
						  <tr role="row" align="center" class="text_center">
							<th rowspan="1" colspan="1" style="width: 68px;" aria-label="">
							</th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Time</th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Customer name</th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Table Number  </th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Status </th>						
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Final Total </th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Original Total </th>
							
						  </tr>
						</thead>
						<tbody>
						<?php
							
							
							if($num_rows > 0){
							    
								$total_bils = array();
								
								while($info = mysql_fetch_assoc($ins_qry_data_res)){
								$cnt++;							
									/*
										echo '<pre>';
										print_r($info);
										echo '</pre>';						
									*/
						?>                     
						  <tr class="gradeX odd" role="row">
							<td><?php echo $cnt; //$info['aid']; ?></td>
							<td class="text_center">
								<?php 	echo $mysqldate =  date('dS M Y H:iA', strtotime($info['bill_date']));    ?>
							</td>
							<td class="text_center">
								<?php echo $info['bill_name']; ?>
							</td>
							 <td class="text_center">
								<?php echo $info['bill_table']; ?>
							</td>
							<td align="center">
								<?php 
									echo $info['bill_status']; 
								?>							
							</td>
							<td align="center">
								<?php 
								    $total_bils[] = $info['billid'];
								    echo $total_bill =  get_bill_total($info['billid']);
								?>							
							</td>
							 <td align="center">
								<?php 
								    $total_bils[] = $info['billid'];
								    echo $total_bill =  get_original_bill_total($info['billid']);
								?>							
							</td>
							
							
						  </tr>
						<?php
							}
							
							?>
								<tr role="row" align="center" class="text_center">
									<td colspan="5"></td>
									<td colspan="1">Total  &nbsp;<strong><?php echo $total_bills_amount = total_bills($total_bils);?></strong></td>
									<td colspan="2">&nbsp;<strong><?php echo $total_original_bills_amount = total_original_bills($total_bils);?></strong></td>
							  </tr>
							<?php
							
							
						}else{
								echo '<tr role="row" align="center" class="text_center">';
								echo '<td colspan="8">';
								echo 'No Records Found!';
								echo '</td>';
								echo '</tr>';
						}
					?>
						</tbody>
						</table>
						</div>
						
						<div align="center">    
							<?php 
							  echo $paginator; 
							?>
						</div>
						
						</div>
						</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
			<!--DATA_TABLE_EN-->


            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->

 <?php
	include_once("includes/footer.php");
?>
      
