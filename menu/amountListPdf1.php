<?php
define('FPDF_FONTPATH', 'font/');
include_once('./font/fpdf.php');
include_once("../includes/connection_main.php");
$orderId = isset($_REQUEST['orderId']) ? $_REQUEST['orderId'] : 0;
$totalAmount = 0;
$totalQuantity = 0;
$orderArray = array();
$selectOrder = "SELECT billitem.weight,item.itemname,bill.billid,bill.bill_name,bill_mobile,bill_table,bill.bill_date,bill_time,waiter.waiter,billitem.bim_itemid,billitem.bim_item_quantity,billitem.bim_item_quantity_price,billitem.bim_item_price,billitem.parcel FROM bill_item_master as billitem LEFT JOIN bill_master as bill on bill.billid = billitem.billid LEFT JOIN waiter_master as waiter on bill.bill_waiter = waiter.waiter_id LEFT JOIN item_master as item on billitem.bim_itemid = item.itemid WHERE bill.billid = ".$orderId." ORDER BY parcel asc,billitem.bim_id asc";
$selectOrderRes = mysql_query($selectOrder);
$numRows = mysql_num_rows($selectOrderRes);
$select_bill="SELECT * FROM billdetail";
$sel_bill_details=mysql_query($select_bill);
$fetch_bill_data=mysql_fetch_array($sel_bill_details);


$minHeight = 76.2;
$height = 92;
$rowHeight = 6;

$addRowHeight = $rowHeight * $numRows;
$setHeight = $height + $addRowHeight;

if ($minHeight > $setHeight) {
    $setHeight = $minHeight;
}



$pdf = new FPDF('P', 'mm', array(78.5, $setHeight));   //Create new pdf file 76.2 width
$pdf->Open();     //Open file
$pdf->SetAutoPageBreak(false);  //Disable automatic page break
$pdf->AddFont('estre','','estre.php'); //STRANGELO EDESSA
$pdf->AddFont('vrinda','','ebrima.php'); 
$pdf->AddFont('ebrimabd','','ebrimabd.php'); 
$pdf->AddFont('vrinda','','vrinda.php'); 
$pdf->AddFont('vrindab','','vrindab.php'); 
$pdf->AddPage();  //Add first page

$i = 0;
$yAxis = 19;
$yAxis = $yAxis + $rowHeight;

pageHeader();
$bill = '';
while ($orderRow = mysql_fetch_array($selectOrderRes)) {
	$itemName = $orderRow['itemname'];
    if ($orderRow['weight'] == 1000) {
        $itemName = $itemName ." ". $orderRow['wight'] . "kg";
    } else if (($orderRow['weight'] < 1000) && ( $orderRow['weight'] > 0)) {
        $itemName = $itemName ." ". $orderRow['weight'] . "gm";
    } else if ($orderRow['weight'] == 0) {
        $itemName = $itemName;
    }
    $kgPrice = 0; //$orderRow['kgPrice'];
    $quantity = $orderRow['bim_item_quantity'];
    $waiterName = $orderRow['waiter'];
	$dt = new DateTime($orderRow['bill_date']);
    $startDate = $dt->format('d-m-Y');;
    $startTime = $orderRow['bill_time'];
    $tableId = $orderRow['bill_table'];
        $amount = $orderRow['bim_item_price'];
        $itemPrice = $orderRow['bim_item_quantity_price'];        
   
    $totalQuantity += $orderRow['bim_item_quantity'];
    $totalAmount += $amount;

    $i++;
    $pdf->SetTextColor(0, 0, 0);
    $pdf->SetXY(5, $yAxis + 18);
    $pdf->SetFont('Arial','', 10);
    $pdf->Cell(36, 18, $itemName, 0, 0, 'L');
    $pdf->SetFont('Arial','', 10); 
    if ($orderRow['parcel'] == '1') {
        $pdf->Image('./images/parcel.png', $pdf->GetX(),$pdf->GetY(),4,5);
        $pdf->Cell(11, 18, $quantity, 0, 0, 'R');
    } else {
        $pdf->Cell(11, 18, $quantity, 0, 0, 'R');
    }
    $pdf->Cell(10, 18, $itemPrice, 0, 0, 'R');
    $pdf->Cell(12, 18, $amount, 0, 0, 'R');
    $yAxis += $rowHeight;
    $i++;
    $bill = $orderRow['billid'];
	$bill_name =$orderRow['bill_name'];
}
$billnumber=$bill;
$vat=$fetch_bill_data['vat'];
$other_tax= $fetch_bill_data['othertax'];
$tax3_value= $fetch_bill_data['tax3_value'];
$tax4_value= $fetch_bill_data['tax4_value'];
$tin_no=$fetch_bill_data['tinno'];
$st_no=$fetch_bill_data['stno'];

/*$updateOrdredItems = "UPDATE bill_master 
                         SET t_status = 'C'
                       WHERE billid = " . $orderId;
$updateOrdredItemsRes = mysql_query($updateOrdredItems); 
*/

$pdf->SetTextColor(0, 0, 0);
//$pdf->SetFont('Arial', 'B', 15);
$pdf->SetFont('Arial','', 15);
$pdf->SetXY(40, 25);
$pdf->Cell(17.5, 10, $waiterName, 1, 0, 'C');
$pdf->SetXY(57.5, 25);
$pdf->Cell(17.6, 10, $tableId, 1, 0, 'C');
//$pdf->SetFont('Arial', 'B', 7);

	$pdf->SetFont('Arial','', 10);
	$pdf->SetXY(17, 27);
	$pdf->Cell(23, 10, $startDate, 0, 0, 'L');


if($billnumber)
{
	$pdf->SetFont('Arial','', 10);
	$pdf->SetXY(20, 23);
	$pdf->Cell(23, 10, $billnumber, 0, 0, 'L');
}

if($bill_name != "")
{
	$pdf->SetFont('Arial','', 12);
	$pdf->SetXY(5, 33);	
	$pdf->Cell(35, 15, 'Bill Name:', 0, 0, 'L');
	$pdf->SetXY(25, 33);
	$pdf->Cell(23, 15, $bill_name, 0, 0, 'L');
}

$pdf->SetTextColor(0, 0, 0);
$pdf->SetFont('Arial','', 11);
$pdf->SetXY(3, $yAxis + 21);
$pdf->Line(44, $yAxis + 25, 53, $yAxis + 25);
$pdf->Line(64, $yAxis + 25, 73, $yAxis + 25);
//$pdf->SetFont('Arial', 'B', 12);
$pdf->SetFont('Arial','', 10);
$pdf->Cell(36, 14, 'Total', 0, 0, 'R', 0);
$pdf->Cell(13, 14, $totalQuantity, 0, 0, 'R', 0);
$pdf->SetFont('Arial', 'B', 15);
//$pdf->SetFont('Arial','', 14);
$pdf->Cell(10.5, 5, '', 0, 0, 'C', 0);
//$pdf->Image('../images/rupee.png',$pdf->GetX()-2,$pdf->GetX()+25,4,4);
$rupeesIcon = '../images/rupee.png';
$pdf->Cell( 12, 15, $pdf->Image($rupeesIcon, $pdf->GetX()-4, $pdf->GetY()+5.5, 3.7), 0, 0, 'L', 0 );
$pdf->Cell(1, 15, $totalAmount, 0, 0, 'R', 0);

if($vat != '')
{
	if($vat !='' && $other_tax !='' && $tax3_value !='' && $tax4_value !='')
	{
		$vat_amt=($totalAmount * $vat)/100;
		$other_tax_amt=($totalAmount * $other_tax)/100;
		$tax3_amt=($totalAmount * $tax3_value)/100;
		$tax4_amt=($totalAmount * $tax4_value)/100;
	}	
	else 
	{		
		$vat_amt=($totalAmount * $vat)/100;
		$tax3_amt=($totalAmount * $tax3_value)/100;
	}
}
if($vat != '')
{
	$add_tax=$vat_amt + $other_tax_amt + $tax3_amt + $tax4_amt;
	$tot_amount=$totalAmount + $add_tax;
	
}
else
{
		$tot_amount=$totalAmount;
}






if($fetch_bill_data['vat'] != '' && $fetch_bill_data['vat'] != 0)
{	
	//$pdf->SetXY(42, $yAxis + 26);
	//$pdf->SetXY(30, $yAxis + 26);
	$pdf->SetXY(40, $yAxis + 26);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(45, 15, $fetch_bill_data["tax1_label"] ,        0, 0, 'L', 0);
	//$pdf->SetXY(58, $yAxis + 26);
	//$pdf->SetXY(45, $yAxis + 26);
	$pdf->SetXY(55, $yAxis + 26);
	$pdf->Cell(45, 15, $vat.''.'%',        0, 0, 'L', 0);
	//$pdf->SetXY(65, $yAxis + 26);
	//$pdf->SetXY(53, $yAxis + 26);
	$pdf->SetXY(70, $yAxis + 26);
	//$pdf->Cell(45, 15, '('.round($vat_amt).')',        0, 0, 'L', 0);
	$pdf->Cell(45, 15, round($vat_amt),        0, 0, 'L', 0);
}

if($fetch_bill_data['othertax'] != '' && $fetch_bill_data['othertax'] != 0 )
{
	//$pdf->SetXY(42, $yAxis + 30);
	//$pdf->SetXY(30, $yAxis + 30);
	$pdf->SetXY(40, $yAxis + 30);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(45, 15, $fetch_bill_data["tax2_label"] ,        0, 0, 'L', 0);
	//$pdf->SetXY(58, $yAxis + 30);
	//$pdf->SetXY(45, $yAxis + 30);
	$pdf->SetXY(55, $yAxis + 30);
	$pdf->Cell(45, 15, $other_tax.''.'%',        0, 0, 'L', 0);
	//$pdf->SetXY(65, $yAxis + 30);
	//$pdf->SetXY(53, $yAxis + 30);
	$pdf->SetXY(70, $yAxis + 30);
	//$pdf->Cell(45, 15, '('.round($other_tax_amt).')',        0, 0, 'L', 0);
	$pdf->Cell(45, 15, round($other_tax_amt),        0, 0, 'L', 0);
}




if($fetch_bill_data['tax3_value'] != '' && $fetch_bill_data['tax3_value'] != 0)
{
	//$pdf->SetXY(42, $yAxis + 30);
	//$pdf->SetXY(30, $yAxis + 30);
	$pdf->SetXY(40, $yAxis + 30);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(45, 23, $fetch_bill_data["tax3_label"] ,        0, 0, 'L', 0);
	//$pdf->SetXY(58, $yAxis + 30);
	//$pdf->SetXY(45, $yAxis + 30);
	$pdf->SetXY(55, $yAxis + 30);
	$pdf->Cell(45, 23, $tax3_value.''.'%',        0, 0, 'L', 0);
	//$pdf->SetXY(65, $yAxis + 30);
	//$pdf->SetXY(53, $yAxis + 30);
	$pdf->SetXY(70, $yAxis + 30);
	//$pdf->Cell(45, 15, '('.round($other_tax_amt).')',        0, 0, 'L', 0);
	$pdf->Cell(45, 23, round($tax3_amt),        0, 0, 'L', 0);
}



if($fetch_bill_data['tax4_value'] != '' &&  $fetch_bill_data['tax4_value'] != 0 )
{
	//$pdf->SetXY(42, $yAxis + 30);
	//$pdf->SetXY(30, $yAxis + 30);
	$pdf->SetXY(40, $yAxis + 30);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(45, 30, $fetch_bill_data["tax4_label"] ,        0, 0, 'L', 0);
	//$pdf->SetXY(58, $yAxis + 30);
	//$pdf->SetXY(45, $yAxis + 30);
	$pdf->SetXY(55, $yAxis + 30);
	$pdf->Cell(45, 30, $tax4_value.''.'%',        0, 0, 'L', 0);
	//$pdf->SetXY(65, $yAxis + 30);
	//$pdf->SetXY(53, $yAxis + 30);
	$pdf->SetXY(70, $yAxis + 30);
	//$pdf->Cell(45, 15, '('.round($other_tax_amt).')',        0, 0, 'L', 0);
	$pdf->Cell(45, 30, round($tax4_amt),        0, 0, 'L', 0);
}

if($fetch_bill_data['vat'] != '')
{
	//$pdf->SetXY(22, $yAxis + 40);
	$pdf->SetXY(21, $yAxis + 40);
	$pdf->SetFont('Arial', 'B', 17);
	$pdf->Cell(35, 21, 'Grand Total: ',        0, 0, 'L', 0);
	$rupeesIcon = '../images/rupee.png';
	$pdf->Cell( 40, 40, $pdf->Image($rupeesIcon, $pdf->GetX()+2, $pdf->GetY()+8.5, 4.2), 0, 0, 'L', 0 );
	//$pdf->SetXY(58, $yAxis + 40);
	$pdf->SetXY(62, $yAxis + 40);
	$pdf->Cell(45, 21, round($tot_amount),        0, 0, 'L', 0);
	
}


if($fetch_bill_data['tinno'] != '')
{
	
	$pdf->SetXY(5, $yAxis + 33);
	$pdf->SetFont('Arial','', 8);
	$pdf->Cell(45, 46, 'Tin No:',        0, 0, 'L', 0);
	$pdf->SetXY(16, $yAxis + 33);
	$pdf->Cell(45, 46, $tin_no,        0, 0, 'L', 0);

}

if($fetch_bill_data['stno'] != '')
{
	$pdf->SetXY(5, $yAxis + 37);
	$pdf->SetFont('Arial', '', 8);
	$pdf->Cell(45, 46, 'St No:',        0, 0, 'L', 0);
	$pdf->SetXY(16, $yAxis + 37);
	$pdf->Cell(45, 46, $st_no,        0, 0, 'L', 0);
}

$pdf->SetXY(5, $yAxis + 52);
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(45, 25, 'E. & O. E. , Subject to Rajkot Jurisdiction',        0, 0, 'L', 0);


//$pdf->SetXY(5, 3);
//$pdf->Cell(70.2, $yAxis + 55, '', 1, 0, 'C'); 
$pdf->Output();

function pageHeader() {
    global $pdf;

    $pdf->setFont('Arial','B','26');
	$pdf->SetXY(12,7);
	$pdf->Cell(56,4, 'SHIV', 0, 0, 'C');
	$pdf->setFont('Arial','B','14');
	$pdf->SetXY(12,13);
	$pdf->Cell(56,4, 'ICE-CREAM & FAST-FOOD', 0, 0, 'C');
	$pdf->SetFont('Arial','B',8);
    $pdf->SetXY(5, 18);
    $pdf->Cell(70.2, 3, 'Race Course Ring Road, Rajkot-1.', 0, 0, 'C');        
    $pdf->SetFont('Arial','', 10);
    $pdf->SetXY(5, 25);
    $pdf->Cell(35, 10, '', 1, 0, 'L');
    $pdf->SetXY(5, 23);
    $pdf->Cell(35, 10, 'Bill No.:', 0, 0, 'L');
    $pdf->SetXY(5, 27);
    $pdf->Cell(35, 10, 'Date:', 0, 0, 'L');
    $pdf->SetTextColor(0, 0, 0);    
    $pdf->SetFont('Arial','', 10);
    $pdf->SetXY(5, 37);
    $pdf->Cell(36, 19, 'Item', 0, 0, 'C', 0);
    $pdf->Cell(11, 19, 'Qty', 0, 0, 'R', 0);
    $pdf->Cell(10.5, 19, 'Rate', 0, 0, 'R', 0);
    $pdf->Cell(12.6, 19, 'Amt', 0, 0, 'C', 0);
    $pdf->Line(19, 49, 28, 49);
    $pdf->Line(44, 49, 53, 49);
    $pdf->Line(55, 49, 62, 49);
    $pdf->Line(65, 49, 73, 49);
}

$pdf->Output('pdf/'.$orderId.'.pdf','F');
?>
