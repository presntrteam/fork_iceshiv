<?php
	$filename = "Dashboard";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");

	$itemid = $_REQUEST['iid'];
	$myaction = isset($_REQUEST["myaction"])?strtolower(trim($_REQUEST["myaction"])):"";
	
	$sel_data_qry = "SELECT * FROM item_master WHERE itemid = '".$itemid."' ";
	if ($ins_qry_data_res = mysql_query($sel_data_qry))
	{ 
		$num_rows = mysql_num_rows($ins_qry_data_res);
	}
	
	if($num_rows){
	
		$info_website = mysql_fetch_assoc($ins_qry_data_res);
		//echo $info_website['website_name'];
	}

	
	
	
?>
  

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?=$filename?>
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?=$filename?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
             	
			 <!--Add_Item_ST-->
			 	<?php
				//print_r($info_website);
				?>
				<div class="col-md-6">
			 <form role="form" class="form-horizontal"  action="item_db.php" name="frm_additem" id="frm_additem" method="post" enctype="multipart/form-data" >
			 	<input type="hidden" name="myaction" id="myaction"  value="edit" />
			 	<input type="hidden" name="itemid" id="itemid"  value="<?=$info_website['itemid']?>" />
			 
				  <div class="box-body">
	
					<div class="form-group">
						  <label class="col-sm-3 control-label" for="inputEmail3">Item Name</label>
						  <div class="col-sm-9">
							  <input type="text" placeholder="Enter Item Name" id="itemname" name="itemname" class="form-control"  value="<?=$info_website['itemname']?>" required >
							</div>
					</div>
					
					 <div class="form-group">
						  <label class="col-sm-3 control-label" for="inputEmail3">Item Price</label>
						  <div class="col-sm-9">
							  <input type="text" placeholder="Enter Item Price" id="itemprice" name="itemprice" class="form-control" value="<?=$info_website['itemprice']?>" required>
							</div>
					</div>
                    
                    <div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">1 Kg Price:</label>
						<div class="col-sm-9">
						<input id="kgPrice" type="number" value="<?=$info_website['kgPrice']?>" name="kgPrice" >
						Rs.
						</div>
					</div>
                    
                    <div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Price Type:</label>
						<div class="col-sm-9">
							<select id="price_type" name="price_type">
								<option <?php echo ( $info_website['price_type'] == "setIceCreamItemId" ) ? "selected" : "" ; ?> value="setIceCreamItemId">Unit and 1 Kg</option>
								<option <?php echo ( $info_website['price_type'] == "setItemId" ) ? "selected" : "" ; ?> value="setItemId">Only Unit</option>
							</select>
						</div>
					</div>
                    
                    <div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Display Sequence:</label>
						<div class="col-sm-9">
							<input type="number" value="<?=$info_website['itemSeq']?>" name="itemSeq" id="itemSeq">
						</div>
					</div>
                    
                    <div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Background Color:</label>
						<div class="col-sm-9">
							<input type="color" value="<?=$info_website['backColor']?>" id="backColor" class="color" name="backColor" >
						</div>
					</div>
                    
                    <div class="form-group">
						<label class="col-sm-3 control-label" for="inputEmail3">Visible:</label>
						<div class="col-sm-9">
							<select name="visible" id="visible">
								<option <?php echo ( $info_website['visible'] == "0" ) ? "selected" : "" ; ?> value="0">Hiden</option>
								<option <?php echo ( $info_website['visible'] == "1" ) ? "selected" : "" ; ?>  value="1">Visible</option>
							</select> 
						</div>
					</div>
					
					 <div class="form-group">
						  <label class="col-sm-3 control-label" for="inputEmail3">Notes</label>
						  <div class="col-sm-9">
							  <input type="text" placeholder="Enter Item Price" id="itemnotes" name="itemnotes" class="form-control"  value="<?=$info_website['itemnotes']?>" >
							</div>
					</div>
					
					<div class="form-group">
                      <label class="col-sm-3 for="exampleInputFile">File input</label>
                      <div class="col-sm-9">
                      	<input type="file" id="item_image" name="item_image" >
                      	<p class="help-block">Please select image for product.</p>
					  </div>	
                    </div>
                    
                    <div class="form-group">
                    <label class="col-sm-3 for="exampleInputFile">&nbsp;</label>
                    <div class="col-sm-9">
                    <?php 
					 	$item_img =  $info_website['item_image'];
						if($item_img != ""){
					 ?>
                         <a href="item_db.php?myaction=remove_img&itemid=<?php echo $info_website['itemid'];?>" ><strong>REMOVE IMAGE (X)</strong></a><br/><br/>
                		 <a class="img_gallery" href="items_uploads/<?php echo $item_img;?>">
                         	<img src="image_thumb.php?src=items_uploads/<?php echo $item_img;?>&q=100&h=120" />
                         </a>
                            
                     <?php
						}
					 ?>
                     </div>	
                    </div>
                    
				  </div>
              <!-- /.box-body -->

              <div class="box-footer col-md-12" align="center">
              <br/>
                <button class="btn btn-primary" type="submit">Update</button> &nbsp;  &nbsp; 
                <button class="btn btn-default" type="submit">Cancle</button>
              </div>
            </form>
				<div class="clear">&nbsp;&nbsp;</div>
			</div>
			 <!--Add_Item_EN-->
				
            </div><!-- /.box-body -->
            <div class="box-footer">
              
            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->

 <?php
	include_once("includes/footer.php");
?>
      
