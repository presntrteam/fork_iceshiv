	      <!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          
		    <?php
		  	function active_class($fname)
			{
					$currnt_filename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
					if( $fname == $currnt_filename ){
						echo 'active';
					}
			}
		  ?>
		  
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
           
            <li class="<?php active_class("dashbord.php"); ?> <?php active_class("settings.php"); ?> treeview">
              <a href="dashbord.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php active_class("settings.php"); ?>"><a href="settings.php"><i class="fa fa-circle-o"></i> My Account</a></li>
              </ul>
            </li>
			
			<li class="<?php active_class("menu"); ?>">
				<a href="<?=$ROOT_SITE_URL?>menu/index.php?waiter=1&table=1&tablePart=A">
					<i class="fa fa-link" aria-hidden="true"></i><span>Shiv Menu</span>
				</a>
			</li>
			
			<!--Manage_Item_ST-->
            <li class="<?php active_class("list_items.php"); ?> <?php active_class("list_tables.php"); ?>  treeview">
              <a href="#">
                <i class="fa fa-cog"></i> <span>Manage Items</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="<?php active_class("list_items.php"); ?>">
                	<a href="list_items.php"><i class="fa fa-circle-o"></i>Manage Food Items</a>
                </li>
                
                 <li class="<?php active_class("list_tables.php"); ?>">
                	<a href="list_tables.php"><i class="fa fa-circle-o"></i>Manage Tables</a>
                </li>
                
              </ul>
            </li>
            <!--Manage_Item_EN-->
			<?php if($_SESSION['user_type'] == 1)
			{ ?>
			
			<!--Manage_Bill_ST-->
			   <li class="<?php active_class("list_bill.php"); ?> <?php active_class("add_bill.php"); ?> <?php active_class("list_fix_bill.php"); ?> treeview">
				  <a href="#">
					<i class="fa fa-cutlery"></i> <span>Manage Bills</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
				  <ul class="treeview-menu">
					<li class="<?php active_class("list_bill.php"); ?>"><a href="list_bill.php"><i class="fa fa-circle-o"></i>Show Bills</a></li>
					<li class="<?php active_class("add_bill.php"); ?>"><a href="add_bill.php"><i class="fa fa-circle-o"></i>Add New Bill</a></li>
					<?php /*?><li class="<?php active_class("list_fix_bill.php"); ?>"><a href="list_fix_bill.php"><i class="fa fa-adjust"></i>Bills Deduction</a></li><?php */?>
				  </ul>
				</li>
			<!--Manage_Bill_EN-->
            <?php } ?>
			<?php if($_SESSION['user_type'] == 1)
			{ ?>
			<!--Manage_Bill_ST-->
			   <li class="<?php active_class("temp_list_bill.php"); ?> <?php active_class("temp_add_bill.php"); ?> <?php active_class("temp_edit_bill.php"); ?>  treeview">
				  <a href="#">
					<i class="fa fa-cutlery"></i> <span>Manage Temporary Bills</span> <i class="fa fa-angle-left pull-right"></i>
				  </a>
				  <ul class="treeview-menu">
					<li class="<?php active_class("temp_list_bill.php"); ?>"><a href="temp_list_bill.php"><i class="fa fa-circle-o"></i>Show Bills</a></li>
					<li class="<?php active_class("temp_add_bill.php"); ?>"><a href="temp_add_bill.php"><i class="fa fa-circle-o"></i>Add New Bill</a></li>
				  </ul>
				</li>
			<!--Manage_Bill_EN-->
			<?php } ?>
			<li class="<?php active_class("list-waiter.php"); ?>">
				<a href="list-waiter.php">
					<i class="fa fa-users" aria-hidden="true"></i><span>Manage Waiter</span>
				</a>
			</li>
			<li class="<?php active_class("item_wise_result.php"); ?>">
				<a href="item_wise_result.php">
					<i class="fa fa-random" aria-hidden="true"></i><span>Item wise list</span>
				</a>
			</li>
            <li class="<?php active_class("close_table.php"); ?>">
				<a href="close_table.php">
					<i class="fa fa-random" aria-hidden="true"></i><span>Tables</span>
				</a>
			</li>
            <?php if($_SESSION['user_type'] == 1)
			{ ?>
            <li class="<?php active_class("export_bill.php"); ?>">
				<a href="export_bill.php">
					<i class="fa fa-random" aria-hidden="true"></i><span>Export Bill</span>
				</a>
			</li>
            <?php } ?>
			<!--Converstin_ST-->
			<?php if($_SESSION['user_type'] == 1)
			{ ?>
				<li>
					<a href="display-possible-conversion.php">
						<i class="fa fa-random" aria-hidden="true"></i><span>Display Possible Conversion</span>
					</a>
				</li>
			<?php } ?>
			<!--Converstin_EN-->			
	<!--Logout_ST-->
			<?php if($_SESSION['user_type'] == 1)
			{ ?>
			<li>
				<a href="db_bkp.php">
					<i class="fa fa-random"></i> <span>Database Backup</span>
				</a>
			</li>
			<?php } ?>
			<!--Logout_EN-->
			<?php if($_SESSION['user_type'] == 1)
			{ ?>
			<li class="<?php active_class("vat_bill_detail.php") ?>">
				<a href="vat_bill_detail.php">
					<i class="fa fa-random"></i> <span>Bill Detail (VAT/TAX etc)</span>
				</a>
			</li>
            <?php } ?>
	
			<!--Logout_ST-->
			<li>
				<a href="logout.php">
					<i class="fa fa-power-off"></i> <span>Logout</span>
				</a>
			</li>
			<!--Logout_EN-->

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
	  <!-- =============================================== -->
