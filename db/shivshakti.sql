-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2017 at 06:31 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shivshakti`
--

-- --------------------------------------------------------

--
-- Table structure for table `billdetail`
--

CREATE TABLE `billdetail` (
  `billdetailid` int(2) NOT NULL,
  `billstart` int(10) NOT NULL,
  `vat` int(10) NOT NULL,
  `othertax` int(10) NOT NULL,
  `tinno` varchar(100) NOT NULL,
  `stno` varchar(100) NOT NULL,
  `tax1_label` varchar(20) NOT NULL,
  `tax2_label` varchar(20) NOT NULL,
  `tax3_label` varchar(20) NOT NULL,
  `tax4_label` varchar(20) NOT NULL,
  `tax3_value` int(11) NOT NULL,
  `tax4_value` int(11) NOT NULL,
  `billstartval` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billdetail`
--

INSERT INTO `billdetail` (`billdetailid`, `billstart`, `vat`, `othertax`, `tinno`, `stno`, `tax1_label`, `tax2_label`, `tax3_label`, `tax4_label`, `tax3_value`, `tax4_value`, `billstartval`) VALUES
(1, 6000, 10, 11, '222222', '666666', 'VAT', 'Other Tax', 'GST', 'Service', 20, 13, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bill_item_master`
--

CREATE TABLE `bill_item_master` (
  `bim_id` int(10) NOT NULL,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` int(11) NOT NULL,
  `order_time` varchar(255) NOT NULL,
  `user_by` varchar(255) NOT NULL,
  `parcel` enum('0','1') NOT NULL,
  `weight` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bill_item_master_original`
--

CREATE TABLE `bill_item_master_original` (
  `bim_id` int(10) NOT NULL,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bill_master`
--

CREATE TABLE `bill_master` (
  `billid` int(11) NOT NULL,
  `bill_name` varchar(100) DEFAULT NULL,
  `bill_mobile` varchar(50) DEFAULT NULL,
  `bill_table` varchar(100) DEFAULT NULL,
  `bill_waiter` varchar(100) DEFAULT NULL,
  `table_part` varchar(5) DEFAULT NULL,
  `bill_status` varchar(25) DEFAULT NULL,
  `bill_notes` varchar(255) DEFAULT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL DEFAULT '0',
  `t_status` enum('L','C','R') NOT NULL,
  `update_time` time NOT NULL,
  `isvisible` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bill_master_original`
--

CREATE TABLE `bill_master_original` (
  `billid` int(11) NOT NULL,
  `bill_name` varchar(100) NOT NULL,
  `bill_mobile` varchar(50) NOT NULL,
  `bill_table` int(10) NOT NULL,
  `bill_waiter` int(10) NOT NULL,
  `bill_status` varchar(25) NOT NULL,
  `bill_notes` varchar(255) NOT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(10) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `page_name` varchar(50) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE `item_master` (
  `itemid` int(10) NOT NULL,
  `itemname` varchar(100) NOT NULL,
  `kgPrice` varchar(255) NOT NULL,
  `price_type` varchar(255) NOT NULL,
  `itemSeq` int(11) DEFAULT NULL,
  `backColor` varchar(255) NOT NULL,
  `visible` varchar(255) NOT NULL,
  `itemprice` int(11) NOT NULL,
  `itemnotes` varchar(255) NOT NULL,
  `item_image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_master`
--

INSERT INTO `item_master` (`itemid`, `itemname`, `kgPrice`, `price_type`, `itemSeq`, `backColor`, `visible`, `itemprice`, `itemnotes`, `item_image`) VALUES
(1, 'Vanilla', '400', 'setIceCreamItemId', 1, '#d2a8ea', '0', 40, 'Vanilla', '1_1vanilla-bean-ice-cream-2-mark.jpeg'),
(30, 'Ice Cream', '500', 'setIceCreamItemId', 2, '#c0a092', '0', 50, '', '30_44chocolate-mint-stout-ice-cream-tk.jpeg'),
(31, 'Shiv ice cream', '700', 'setIceCreamItemId', 3, '#b1b1b1', '1', 70, '', '31_311.jpg'),
(32, 'Kingcone', '0', 'setItemId', 4, '#9ebdde', '1', 35, '', '32_32kingcone.jpg'),
(33, 'Candy', '0', 'setItemId', 5, '#91e6ec', '1', 30, '', '33_33candy.jpg'),
(34, 'Mava candy', '0', 'setItemId', 6, '#c3c3c3', '1', 40, '', '34_34mava-candy.jpeg'),
(35, 'Sundae', '0', 'setItemId', 7, '#d3625f', '1', 130, '', '35_35sundae.jpeg'),
(36, 'Vanila/Coffee', '0', 'setItemId', 8, '#cbbcb8', '1', 100, '', '36_36cookie-dough-vanilla-milk-shake-simplygloria.com-.jpeg'),
(37, 'Shake', '0', 'setItemId', 9, '#9fc4c4', '1', 120, '', '37_37chocolate-shake2.jpeg'),
(38, 'Shiv sp. Shake', '0', 'setItemId', 10, '#9bcdff', '1', 170, '', '38_38vanilacoffeesp.jpg'),
(39, 'Chickoo shake', '0', 'setItemId', 11, '#b4988b', '1', 60, '', '39_39chikoo-2.jpeg'),
(40, 'Juice', '', 'setIceCreamItemId', 55, '#a2a2a2', '0', 60, '', '40_40download-1.jpg'),
(41, 'Faluda', '0', 'setItemId', 13, '#fdffbb', '1', 90, '', '41_41faluda23.jpg'),
(42, 'Extra Topping', '0', 'setItemId', 14, '#ffa6a8', '1', 40, '', '42_42extopping.jpg'),
(43, 'Half Vanila', '0', 'setItemId', 15, '#c6caff', '1', 20, '', '43_1vanilla-bean-ice-cream-2-mark.jpeg'),
(44, 'Half Ice cream', '0', 'setItemId', 16, '#c1a8a8', '1', 25, '', '44_44chocolate-mint-stout-ice-cream-tk.jpeg'),
(45, 'Half Shiv ice cream', '0', 'setItemId', 17, '#a8bbba', '1', 35, '', '45_311.jpg'),
(46, 'Cold Drink', '0', 'setItemId', 18, '#9ea9d1', '1', 30, '', '46_46coldrink.jpeg'),
(47, 'Min.Water', '0', 'setItemId', 20, '#9fcccc', '1', 20, '', '47_47bisleri1---copy.jpg'),
(48, 'French Fries', '0', 'setItemId', 20, '#f1e9ad', '1', 80, '', '48_48french-fries-1.jpeg'),
(49, 'Chutney', '0', 'setItemId', 21, '#b0d9aa', '1', 10, '', '49_491.jpeg'),
(50, 'Grill', '0', 'setItemId', 22, '#e8bdec', '1', 100, '', '50_50full-sandwich.jpg'),
(51, 'Non Grill', '0', 'setItemId', 23, '#b0a9b4', '1', 95, '', '51_51veg-grill-sandwich-big.jpg'),
(52, 'Burger', '0', 'setItemId', 24, '#dac5da', '1', 90, '', '52_52burger.jpeg'),
(53, 'Club', '0', 'setItemId', 25, '#ede7b8', '1', 150, '', '53_53club.jpg'),
(54, 'Oven Toast', '0', 'setItemId', 26, '#afedd5', '1', 150, '', '54_54soupssaladssandwichescreamcentrelarge.jpeg'),
(55, 'Pizza', '0', 'setItemId', 27, '#bcdb9d', '1', 170, '', '55_55pizzas.jpg'),
(56, 'Shiv Pizza', '0', 'setItemId', 28, '#ffb9bb', '1', 225, '', '56_56shiv-pizza.jpg'),
(57, 'Bread Grill', '0', 'setItemId', 29, '#c5e9fa', '1', 85, '', '57_57breadbuttergril.jpg'),
(58, 'Bread Non Grill', '0', 'setItemId', 30, '#cee0ff', '1', 80, '', '58_58bread-butter-1171149.jpeg'),
(59, 'Garlic Bread', '0', 'setItemId', 31, '#fff1bf', '1', 95, '', '59_59garlicbread.jpeg'),
(60, 'Rice/Bhel', '0', 'setItemId', 32, '#e89fa2', '1', 140, '', '60_60ricebhel.jpeg'),
(61, 'Noodles', '0', 'setItemId', 33, '#d9cab7', '1', 140, '', '61_61schezwan-veg-noodles.jpeg'),
(62, 'M.Dry/Gravy', '0', 'setItemId', 34, '#b3bde1', '1', 140, '', '62_62manchurian-dry.jpeg'),
(63, 'Trip. Sch. Rice', '0', 'setItemId', 35, '#c6e1b5', '1', 220, '', '63_63trip-rice.jpeg'),
(64, 'DF. Faluda', '0', 'setItemId', 37, '#fffec4', '1', 100, '', '64_64faluda23.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `siteadmin`
--

CREATE TABLE `siteadmin` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `adminemail` varchar(50) NOT NULL,
  `user_type` enum('1','2') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siteadmin`
--

INSERT INTO `siteadmin` (`admin_id`, `username`, `password`, `fullname`, `adminemail`, `user_type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'shive admin', 'admin@gmail.com', '1'),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', '', 'user@gmail.com', '2');

-- --------------------------------------------------------

--
-- Table structure for table `table_master`
--

CREATE TABLE `table_master` (
  `tid` int(11) NOT NULL,
  `tname` varchar(20) NOT NULL,
  `backColor` varchar(250) NOT NULL,
  `floor` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_master`
--

INSERT INTO `table_master` (`tid`, `tname`, `backColor`, `floor`) VALUES
(3, '1', '#278ac5', '0'),
(4, '2', '#ea1a29', '0'),
(5, '3', '', '0'),
(6, '4', '', '0'),
(7, '5', '', '0'),
(8, '6', '', '0'),
(9, '7', '', '0'),
(10, '8', '', '0'),
(11, '9', '', '0'),
(12, '10', '', '0'),
(13, '11', '', '0'),
(14, '12', '', '0'),
(15, '13', '', '0'),
(16, '14', '', '0'),
(17, '15', '', '0'),
(18, '16', '', '0'),
(19, '17', '', '0'),
(20, '18', '', '0'),
(21, '19', '', '0'),
(22, '20', '', '0'),
(23, '51', '#7382e8', '1'),
(24, '52', '', '1'),
(25, '53', '', '1'),
(26, '54', '', '1'),
(27, '55', '', '1'),
(28, '56', '', '1'),
(29, '57', '', '1'),
(30, '58', '', '1'),
(31, '59', '', '1'),
(32, '60', '', '1'),
(33, 'TT', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `temp_bill_item_master`
--

CREATE TABLE `temp_bill_item_master` (
  `bim_id` int(10) NOT NULL,
  `billid` int(10) NOT NULL,
  `bim_itemid` int(10) NOT NULL,
  `bim_item_quantity` int(11) NOT NULL,
  `bim_item_quantity_price` int(10) NOT NULL,
  `bim_item_price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_bill_master`
--

CREATE TABLE `temp_bill_master` (
  `billid` int(11) NOT NULL,
  `bill_name` varchar(100) NOT NULL,
  `bill_mobile` varchar(50) NOT NULL,
  `bill_table` int(10) NOT NULL,
  `bill_waiter` int(10) NOT NULL,
  `bill_status` varchar(25) NOT NULL,
  `bill_notes` varchar(255) NOT NULL,
  `bill_date` datetime NOT NULL,
  `bill_time` time NOT NULL,
  `bill_total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `waiter_master`
--

CREATE TABLE `waiter_master` (
  `waiter_id` int(11) NOT NULL,
  `waiter` varchar(20) NOT NULL,
  `floor` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `waiter_master`
--

INSERT INTO `waiter_master` (`waiter_id`, `waiter`, `floor`) VALUES
(1, 'T', '0'),
(2, 'N', '0'),
(3, 'G', '0'),
(4, 'A', '0'),
(5, 'J', '0'),
(6, 'D', '0'),
(7, 'V', '0'),
(8, 'B', '0'),
(9, 'F', '0'),
(10, 'O', '0'),
(11, 'M', '0'),
(12, 'L', '0'),
(13, 'U', '0'),
(14, 'H', '0'),
(15, 'K', '0'),
(16, 'P', '0'),
(17, 'FA', '1'),
(18, 'FB', '1'),
(19, 'FC', '1'),
(20, 'FD', '1'),
(21, 'FE', '1'),
(22, 'FF', '1'),
(23, 'FG', '1'),
(24, 'FH', '1'),
(25, 'FI', '1'),
(26, 'FJ', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billdetail`
--
ALTER TABLE `billdetail`
  ADD PRIMARY KEY (`billdetailid`);

--
-- Indexes for table `bill_item_master`
--
ALTER TABLE `bill_item_master`
  ADD PRIMARY KEY (`bim_id`);

--
-- Indexes for table `bill_item_master_original`
--
ALTER TABLE `bill_item_master_original`
  ADD PRIMARY KEY (`bim_id`);

--
-- Indexes for table `bill_master`
--
ALTER TABLE `bill_master`
  ADD PRIMARY KEY (`billid`);

--
-- Indexes for table `bill_master_original`
--
ALTER TABLE `bill_master_original`
  ADD PRIMARY KEY (`billid`);

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`itemid`);

--
-- Indexes for table `siteadmin`
--
ALTER TABLE `siteadmin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `table_master`
--
ALTER TABLE `table_master`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `temp_bill_item_master`
--
ALTER TABLE `temp_bill_item_master`
  ADD PRIMARY KEY (`bim_id`);

--
-- Indexes for table `temp_bill_master`
--
ALTER TABLE `temp_bill_master`
  ADD PRIMARY KEY (`billid`);

--
-- Indexes for table `waiter_master`
--
ALTER TABLE `waiter_master`
  ADD PRIMARY KEY (`waiter_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billdetail`
--
ALTER TABLE `billdetail`
  MODIFY `billdetailid` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bill_item_master`
--
ALTER TABLE `bill_item_master`
  MODIFY `bim_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_item_master_original`
--
ALTER TABLE `bill_item_master_original`
  MODIFY `bim_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_master`
--
ALTER TABLE `bill_master`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_master_original`
--
ALTER TABLE `bill_master_original`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `item_master`
--
ALTER TABLE `item_master`
  MODIFY `itemid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `siteadmin`
--
ALTER TABLE `siteadmin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `table_master`
--
ALTER TABLE `table_master`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `temp_bill_item_master`
--
ALTER TABLE `temp_bill_item_master`
  MODIFY `bim_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `temp_bill_master`
--
ALTER TABLE `temp_bill_master`
  MODIFY `billid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `waiter_master`
--
ALTER TABLE `waiter_master`
  MODIFY `waiter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
