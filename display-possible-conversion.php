<?php
	$filename = "Display Possible Conversion";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");

?>
<!-- Content Wrapper. Contains page content -->
<?php
						if(isset($_REQUEST['err']) && $_REQUEST['err'] == "del"){
							$msg = "Display Possible Conversion Deleted Successfully";
						}
?>

<?php 
						if( $msg != ""  && ( $_REQUEST['err'] == "del" ) ){ 
						?> 
						<?php /*?>
                        	<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
							<h4><i class="icon fa fa-check"></i> <?php echo $msg ; ?>!</h4>
						  </div>
						</div>		
						<?php */?>
                        <script type="text/javascript">
						$( document ).ready(function() {
  							swal("<?php echo $msg; ?>", "", "success")
						 });			
						</script>
						<?php
						} ?>


<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?=$filename?>
      <small>it all starts here</small> </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">
        <?=$filename?>
      </li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">
          <?=$filename?>
        </h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
          <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!--ST-->
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Select Date</h3>
          </div>
          <div class="box-body" >
            <!-- Date -->
            <?php
				//  	printit();
			  ?>
            <form role="form" class="form-horizontal"  action="display-possible-conversion.php" name="frm_additem" id="frm_additem" method="post" >
              <input type="hidden" name="myaction" id="myaction"  value="additem" />
              <input type="hidden" name="itemid" id="itemid"  value="" />
              <input type="hidden" name="billid" id="billid"  value="" />
              <div class="form-group">
                <div class="col-sm-3" align="right">
                  <label class="control-label">Date:</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input required type="text" id="datepicker" name="datepicker" value="<?php echo $_REQUEST['datepicker']; ?>" class="form-control pull-right">
                  </div>
                </div>
                <div class="col-sm-3">
                  <button class="btn btn-primary" type="submit">Show Bill</button>
                </div>
                <!-- /.input group -->
              </div>
            </form>

	<form role="form" class="form-horizontal"  action="display-possible-conversion-two.php" name="form_amount_percent" id="form_amount_percent" method="post" >
 			<?php          
		   if(isset($_REQUEST['datepicker']) && $_REQUEST['datepicker'] != "" ){ 
		   ?>
		    <div id="show_conv_amount"  style="margin-top:50px;"> 
              <input type="hidden" name="myaction" id="myaction"  value="additem" />
              <input type="hidden" name="itemid" id="itemid"  value="" />
			  <input type="hidden"  id="datepicker" name="datepicker" value="<?php echo $_REQUEST['datepicker']; ?>"  />
              <div class="form-group">
                <div class="col-sm-3" align="right">
                  <label class="control-label">Current Amount :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-inr" aria-hidden="true"></i></div>
                    <input type="text" id="current_amount" name="current_amount" value="" class="form-control pull-right">
                  </div>
                </div>
               
                <!-- /.input group -->
              </div>
			  
			  <div class="form-group">
                <div class="col-sm-3" align="right">
                  <label class="control-label">Make :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-percent" aria-hidden="true"></i></div>
                    <input type="text" id="amount_percent" name="amount_percent" class="form-control pull-right">
                  </div>
                </div>
               
                <!-- /.input group -->
              </div>
			  
			  <div class="form-group">
                <div class="col-sm-3" align="right">
                  <label class="control-label">Expected Amount :</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon" style="background-color:#00A65A; color:#F7F5F5;"><i class="fa fa-inr" aria-hidden="true"></i></div>
                    <input type="text" id="amount_val" name="amount_val"  class="form-control pull-right" required >
                  </div>
                </div>
				
	       
		   		<div class="col-sm-12" style="margin:20px 0px;">
					<div class="col-sm-3">&nbsp;</div>
					<button class="btn btn-primary" type="button" id="display_conversion">Display Possible Conversion</button>
					&nbsp;&nbsp;
					<button onclick="location.href = 'dashbord.php';" class="btn btn-default" type="button">Cancel</button>
				</div>
					
                <!-- /.input group -->
              </div>
			  
            
			</div>
			<?php } ?>
					
            <!--Listing_ST-->
            <?php
			if(isset($_REQUEST['datepicker']) && $_REQUEST['datepicker'] != "" ){ 
			
						$datepicker = isset($_REQUEST["datepicker"])?strtolower(trim($_REQUEST["datepicker"])):"";
				
						// **************** MyCondition ST ***********************
							$myCondition = Array();
							if(isset($_REQUEST["keywords_search"]) && strlen($_REQUEST["keywords_search"]) > 0)
							{
								$Keywords_Search = $_REQUEST["keywords_search"];
								array_push($myCondition," username LIKE '%".setGPC($Keywords_Search,"")."%' ");	
							}
							else
								$Keywords_Search = ""; 
							
							//array_push($myCondition," bill_date like '%".$datepicker_search."%' ");	
							//array_push($myCondition," bill_status like '%nottaken%' ");
							
							
							if(isset($_REQUEST["datepicker"]) && strlen($_REQUEST["datepicker"]) > 0)
							{

								$datepicker_search = date("Y-m-d",strtotime($_REQUEST["datepicker"]));
								array_push($myCondition," bill_date like '%".$datepicker_search."%' ");	
							}
							else
								$datepicker_search = ""; 

							array_push($myCondition," bill_total > 0 ");
							
							if(count($myCondition) > 0)
								$myCondition = " WHERE ".implode(" AND ", $myCondition);
							else
								$myCondition = "";
							
						// **************** MyCondition EN ***********************
						$sel_qry = "SELECT count(*) as cnt FROM bill_master $myCondition ";
						if ($ins_qry_res = mysql_query($sel_qry))
						{
							$fetch_rec = mysql_fetch_assoc($ins_qry_res);
							$num_rows = $fetch_rec["cnt"];
						}
						
						$itemsPerPage = 200;
						if(isset($_GET['page']) && $_GET['page'] != ""){
							$currentPage = $_GET['page'];
							
						}else{
							
							$currentPage = '0';
						}
						$totalItems = $num_rows;
						if($currentPage >0 ){
							$startAt = $itemsPerPage * ($currentPage - 1);
						}else{
							$startAt = 0;
						}	
						$cnt = $startAt;
						$urlPattern = 'list_bill.php?page=(:num)';
						
						$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
						
						
						//$sel_data_qry = "SELECT * FROM 	bill_master $myCondition  limit $startAt, $itemsPerPage";
						$sel_data_qry = "SELECT * FROM 	bill_master $myCondition";
						if ($ins_qry_data_res = mysql_query($sel_data_qry))
						{ 	// echo "New record created successfully";
							$num_rows = mysql_num_rows($ins_qry_data_res);
							
						}else{  echo "Error: " . $sel_qry . "<br>" . mysql_error($db);  }
			
			
			?>
            <div class="table-scrollable">
              	 <div class="alert alert-info alert-dismissible bg-aqua disabled color-palette">
						<h4>
							<i class="icon fa fa-warning"></i> STEP-1 &nbsp;&nbsp; 
							<span>This is original records for selected date.</span>
						</h4>
				</div>
                <table id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" role="grid" aria-describedby="sample_1_info">
                  <thead>
                    <tr role="row" align="center" class="text_center">
						  <th rowspan="1" colspan="1" style="width: 68px;" aria-label=""> </th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Time</th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Customer name</th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Table Number </th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Status </th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Quantity</th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Total </th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> View </th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> Delete</th>
						  <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center">
	                            <input type="checkbox" name="checkAll" id="checkAll"   />
    	                       	 &nbsp;
                                Add to<br/>template
                           </th>
                          <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center">Remove</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
					if($num_rows > 0){
										
							$taken_home = 0;
							$not_taken_home = 0;					
							
							$total_item = 0;
							$total_item_qnt = 0;

							$total_bils = array();
							while($info = mysql_fetch_assoc($ins_qry_data_res)){
								$cnt++;							
								/*
									echo '<pre>';
									print_r($info);
									echo '</pre>';						
								*/
								$hometaken = "0";
								if($info['bill_status'] == "billtaken"){
									$hometaken = "1";	
								}
						?>
				<tr <?php if($hometaken == "1"){ echo 'style="background-color:#D2D6DE;"';}?>  class="gradeX" role="row">
				  <td>
					<?php //echo $cnt; //$info['aid']; ?><?php echo $info['billid']; ?>
				  </td>
				  <td class="text_center"><?php echo $mysqldate =  date('dS M Y', strtotime($info['bill_date']));    ?>
				  </td>
				  <td class="text_center"><?php echo $info['bill_name']; ?> </td>
				  <td class="text_center"><?php echo $info['bill_table']; ?> </td>
				  <td align="center">
					<?php if($info['bill_status'] == "billtaken"){ ?>
                    		Bill Taken Home	
                   	  <?php }else {  ?>	
	                      Not Taken Home
                   	  <?php }  ?>	
				  </td>
                  <td align="right">
					<?php 
                           echo $total_bill =  get_bill_item_quantity($info['billid']);
						   
						   $total_bill_array =  get_bill_item_quantity_new($info['billid']);
						   //print_r($total_bill_array);
						   
							$total_item = $total_item + $total_bill_array['item_tot'];
							$total_item_qnt = $total_item + $total_bill_array['item_qnt_tot'];
							$arr[]=$total_bill_array['item_qnt_tot'];
							
	              ?>
				  </td>
				  <td align="right">
					<?php 
                            $total_bils[] = $info['billid'];
                            echo $total_bill =  get_bill_total($info['billid']);
                    ?>
				  </td>
				  <td align="center"><a href="edit_bill.php?b=<?php echo $info['billid']; ?>" style="text-decoration:none;"> <span class="label label-sm label-success"> <strong>View </strong></span> </a> </td>
				  <td align="center">
				  <a href="javascript:setAction('delete_one','<?php echo $info['billid']; ?>');"  style="text-decoration:none;"> <span class="label label-sm label-success"> <strong>Delete</strong></span> </a> 
				  </td>
				  
				  <td align="center">
					<input type="checkbox" class="case" id="addtotemplate" name="addtotemplate[]" value="<?php echo $info['billid']; ?>" />
					<input type="hidden" name="addtotemplate_billtotal[<?php echo $info['billid']; ?>]" value="<?php echo $total_bill; ?>" />
                    	<?php /*?>
                    	&nbsp;
                       <a href="javascript:void(0);" onclick="javascript:setAction('insert_one','<?php echo $info['billid']; ?>');"  style="text-decoration:none;"> 
                         <span class="label label-sm label-success"> <strong>Add to Template</strong></span> 
                       </a>
					   <?php */?>
                      
				  </td>
				  <td>
                   	  <?php if($hometaken == "0"){ ?>	
		                  <button type="button" class="removebutton  btn-instagram" data-ftot="<?php echo $total_bill_array['item_qnt_tot']; ?>" data-qty="<?php echo $total_bill_array['item_tot']; ?>" data-amount="<?php echo get_bill_total($info['billid']) ?>" title="Remove this row"><strong>Remove</strong></button>
                          <input type="hidden" name="not_taken_home[]" id="not_taken_home" value="<?php echo $total_bill; ?>" >
                          <input type="hidden" id="order_list" name="order_list[]" value="<?php echo $info['billid']; ?>"  />
                   	  <?php }else if($hometaken == "1"){  ?>	
                          <input type="hidden" name="taken_home[]" id="taken_home" value="<?php echo $total_bill; ?>" >
                   	  <?php }  ?>	

                  </td>
				</tr>
				<?php
						}
												
					?>
                    <tr><td colspan="8">&nbsp;</td></tr>
                    <tr role="row" align="center" class="text_center" style="padding-top:50px;">
                      <td colspan="5"></td>
                      <td colspan="1"align="right"><strong ><?php echo "<span id='update-qty'>".$total_item."</span> (<span id='ftot'>".array_sum($arr)."</span> qty)"; ?></strong></td>
                      <td colspan="1" align="right"><strong id="update-tot"><?php echo $total_bills_amount = total_bills($total_bils);?></strong></td>
                      <td colspan="2">&nbsp;</td>
                      <td colspan="1">
                      		<button id="add_to_template" type="button" class="btn btn-primary">Add to template</button>
                      </td>
                      <td colspan="2"></td>
                    </tr>
                    <?php
												
							}else{
							
							echo '<tr role="row" align="center" class="text_center">';
								echo '<td colspan="8">';
									echo 'No Records Found!';
								echo '</td>';
							echo '</tr>';
							
							}
							?>
                  </tbody>
                </table>
                <input type="hidden" name="myaction">
                <input name="billid" type="hidden" id="billid">
                <input type="hidden" name="sorton" value="<?=$sorton?>">
                <input type="hidden" name="sort" value="<?=$sort?>">

            	</div>
            <?php
				} // End isset date picker 
			  ?>
              <input type="hidden" name="total_remaining_final" id="total_remaining_final" value="" >
              </form>

            <!--Listing_EN-->
          </div>
        </div>
      </div>
      <!--EN-->
      <!-- /.box-body -->
      <div class="box-footer"> </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
	include_once("includes/footer.php");
?>
<script>
	$( document ).ready(function() {
  	
				$('#datepicker').datepicker({
					autoclose: true,
					format: 'dd-mm-yyyy',
					todayHighlight: true
				});

			
				$('#current_amount').val(<?php echo $total_bills_amount; ?> )

					///////
  
				  $("#amount_percent").change(function() { 
						//var $this = $(this);
						
						//alert($('#amount_percent').val());	
						
						 // Ajaxy ST	 ---------------			
						 $.ajax({
							dataType: 'json',
							type: 'POST',
							url: 'ajax_amount_percent_value.php',
							data: $('#form_amount_percent').serialize(),
							success:function(data) 
							{
								  	//console.log(data);	
									$("#amount_val").val(data['exp_amt']);
							}
						
						})
						 // Ajaxy EN	 --------------
														
				  });



// ******** SELECT ALL ST
  $("#checkAll").change(function () {
   		 $("input:checkbox").prop('checked', $(this).prop("checked"));
	});
// ******** SELECT ALL EN	

  });
  
$( document ).ready(function() {
	  
	  $(document).on('click', 'button.removebutton', function () {
			if(!confirm("Are you sure you want to remove this item ?"))
				return false;
			$("#update-tot").html($("#update-tot").html()-$(this).attr('data-amount')+'.00');
			
			$("#update-qty").html($("#update-qty").html()-$(this).attr('data-qty')+'.00');
			$("#ftot").html($("#ftot").html()-$(this).attr('data-ftot')+'.00');
			
			
			$(this).closest('tr').remove();
	  });
	  
//	  $("#form_amount_percent").submit(function() {
	   	
	  $("#add_to_template").click(function() {
		  
		  // Ajax update total  FRM 1 ST ----------------------------------
			$.ajax({
				dataType: 'json',
				type: 'POST',
				url: 'ajax_add_to_template.php',
				data: $('#form_amount_percent').serialize(),
					success:function(data) 
					{
 						 swal("Added To Template successfully", "", "success");
						 $('input:checkbox').removeAttr('checked');
						 	
						 console.log(data);	
						 $('#frm_two_tot').html(data['frm_two_total']);
					}
			
			});
		  // Ajax update total  FRM 1 EN ----------------------------------		  
		  
	  });
	  
	  $("#display_conversion").click(function() {		  
			
			var total_taken_home = 0;
			var total_not_taken_home = 0;
			var pure_not_taken_home = 0;
			
			var taken_home_len = $('input[name="taken_home[]"]').length;
			var not_taken_home_len = $('input[name="not_taken_home[]"]').length;
			
			var per_amount_val = $('#amount_val').val();
			
			$('input[name="taken_home[]"]').each(function() {
				total_taken_home += Number($(this).val());
				console.log($(this).val());
			});
			//console.log("total taken home "+total_taken_home);
			
			$('input[name="not_taken_home[]"]').each(function() {
				total_not_taken_home += Number($(this).val());
				//console.log($(this).val());
			});
			//console.log("total NOT taken home "+total_not_taken_home);
			
			pure_not_taken_home = per_amount_val - total_taken_home;
			console.log(per_amount_val +" - "+ total_taken_home+" = "+pure_not_taken_home);

			if(not_taken_home_len == 0)
			{
				swal("Orders Mismatch!", "You Need More Orders with 'Not Taken Home' status", "error");	
				return false;
			}	
/* 			}else if( Math.abs(per_amount_val) < Math.abs(total_taken_home) ){ // 2000 ( not taken home + taken home ) 
				console.log('here');
				swal({
				  title: "Orders Mismatch!",
				 // text: "You Need More <strong>Not Taken Home</strong> orders.<br/> Expected Amount "+ per_amount_val +"<br/> - Total Home taken "+total_taken_home+"<br/> = Total Remaining "+pure_not_taken_home+"<br/><br/> Currently total <strong>Not Taken Home</strong> "+total_not_taken_home+" <br/> You Need more then "+pure_not_taken_home+" <br/> ",
				  text: "Please increase percentage.<br/> Currently Total Home taken is "+total_taken_home+"<br/> Its more then Expected Amount "+ per_amount_val +" ",
				  html: true,
				  type: "error",
				   
				});	
				return false;
				
			}else if( Math.abs(total_not_taken_home) < Math.abs(pure_not_taken_home) ){ // 2000 ( not taken home + taken home ) 
				console.log('here2');
				swal({
				  title: "Orders Mismatch!",
				 // text: "You Need More <strong>Not Taken Home</strong> orders.<br/> Expected Amount "+ per_amount_val +"<br/> - Total Home taken "+total_taken_home+"<br/> = Total Remaining "+pure_not_taken_home+"<br/><br/> Currently total <strong>Not Taken Home</strong> "+total_not_taken_home+" <br/> You Need more then "+pure_not_taken_home+" <br/> ",
				  text: "You Need More <strong>Not Taken Home</strong> orders.<br/> Expected Amount "+ per_amount_val +"<br/> - Total Home taken "+total_taken_home+"<br/> = Total Remaining "+pure_not_taken_home+"<br/><br/> Currently total <strong>Not Taken Home</strong> "+total_not_taken_home+" <br/> You Need more then "+Math.abs(pure_not_taken_home)+" <br/> ",
				  html: true,
				  type: "error",
				   
				});	
				return false;

			}else if( Math.abs(total_not_taken_home) > Math.abs(pure_not_taken_home) ){ // 2000 ( not taken home + taken home ) */
			else
			{				
				swal({
				  title: "Orders Summery!",
				 // text: "You Need More <strong>Not Taken Home</strong> orders.<br/> Expected Amount "+ per_amount_val +"<br/> - Total Home taken "+total_taken_home+"<br/> = Total Remaining "+pure_not_taken_home+"<br/><br/> Currently total <strong>Not Taken Home</strong> "+total_not_taken_home+" <br/> You Need more then "+pure_not_taken_home+" <br/> ",
				  //text: "Expected Amount "+ per_amount_val +"<br/> - Total Home taken "+total_taken_home+"<br/> = Total Remaining "+pure_not_taken_home+"<br/><br/> You Need "+Math.abs(pure_not_taken_home)+" <br/><br/>Currently <strong>Total order</strong> "+(Math.abs(total_taken_home)+Math.abs(total_not_taken_home))+"  <br/>Currently total <strong>Not Taken Home</strong> "+total_not_taken_home+" <br/>  ",
				  text: "<table width='227px' border='1' align='center'><th></th><th>New</th><th>Old</th><tr><th>Total</th><td align='right'>"+per_amount_val+"</td><td align='right'>"+(Math.abs(total_taken_home)+Math.abs(total_not_taken_home))+"</td></tr> <tr><th>Home</th> <td align='right'>"+total_taken_home+"</td><td align='right'>"+total_taken_home +"</td></tr><tr><th>Not H.</th><td align='right'>"+Math.abs(pure_not_taken_home)+"</td><td align='right'>"+total_not_taken_home+"</td></tr> </table>",
				  html: true,
				  type: "success",
				  showCancelButton: true,
				  confirmButtonText: "Yes, Proceed!",
				  showLoaderOnConfirm: true,
				  closeOnConfirm: false
				  },
			     function(isConfirm){
					if (isConfirm) { 
						setTimeout(function(){
							 $('#total_remaining_final').val(Math.abs(pure_not_taken_home));
							 $( "#form_amount_percent" ).submit();
							 return true;
						}, 2000);
					}
				});	
				
			}	
			
			console.log(Math.abs(total_not_taken_home) +" > "+ Math.abs(pure_not_taken_home) );
			return false;
			
			
	  });
});

function setAction(action,billid)
{
	if(action=="delete")
	{
		document.frm1.user_id.value="";
		document.frm1.myaction.value=action;
		
		document.frm1.action="";
		
		mycount = 0;
		for(i=0;i<document.frm1.elements.length;i++)
		{
			if(document.frm1.elements[i].name=="Rec_Ids[]" && document.frm1.elements[i].checked)
				mycount++;
		}
		if(mycount==0)
		{
			alert("You must check one of the checkboxes.");
			return;
		}
		if(!confirm("Are you sure you want to "+action+" selected users ?"))
			return;
	
		document.frm1.submit();
	}
	else if(action=="delete_one")
	{
		if(!confirm('Are you sure you want to delete this bill ?')) {
			return;
		}
		window.location = "bill_db.php?myaction=delete_one&backto=conversion&dater=<?php echo $_REQUEST['datepicker'] ?>&billid="+billid;
		
	}
	else if(action=="insert_one")
	{	
		/*
		$( document ).ready(function() {
			
			swal({
			  title: "Are you sure?",
			  text: "This will add new template order!",
			  type: "info",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Yes, insert it!",
			  closeOnConfirm: true
			},
			function(isConfirm){
				if (isConfirm) {
					document.form_amount_percent.action="bill_db.php";
					document.form_amount_percent.myaction.value=action;
					document.form_amount_percent.billid.value=billid;	
					document.form_amount_percent.submit();
				}else{
					
				}
			});
			
		});
		*/
		
		
			if(!confirm('Are you sure you want to add this to template ?')) {
				return;
			}
			
			document.frm_additem.action="bill_db.php";
			document.frm_additem.myaction.value=action;
			document.frm_additem.itemid.value=billid;	
			document.frm_additem.billid.value=billid;		
			document.frm_additem.submit();
		
	}
	else if(action=="edit")
	{
		
	}
	else if(action=="add")
	{
	}
	
	return false;
}
</script>
