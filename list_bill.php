<?php
	$filename = "Show Bills";
	
	include_once("includes/header.php");
	include_once("includes/sidebar.php");
	include_once("includes/paginator.php");
	
	$myCondition = Array();
	if(isset($_REQUEST["datepicker"]) && strlen($_REQUEST["datepicker"]) > 0)
	{
		$datepicker_search = date("Y-m-d",strtotime(trim($_REQUEST["datepicker"])));
		array_push($myCondition," bill_date like '%".$datepicker_search."%' ");	
	}
	else
		$datepicker_search = "";

	array_push($myCondition," bill_total > 0 ");

	if(count($myCondition) > 0)
		$myCondition = " WHERE ".implode(" AND ", $myCondition);
	else
		$myCondition = "";
				
	$sel_qry = "SELECT count(*) as cnt FROM bill_master $myCondition ";
	if ($ins_qry_res = mysql_query($sel_qry))
	{
		$fetch_rec = mysql_fetch_assoc($ins_qry_res);
		$num_rows = $fetch_rec["cnt"];
	}
	
	$itemsPerPage = 30;
	if(isset($_GET['page']) && $_GET['page'] != ""){
		$currentPage = $_GET['page'];
		
	}else{
		
		$currentPage = '1';
	}
	$totalItems = $num_rows;
	if($currentPage >0 ){
		$startAt = $itemsPerPage * ($currentPage - 1);
	}else{
		$startAt = 0;
	}	
	$cnt = $startAt;
	$urlPattern = 'list_bill.php?page=(:num)';
	if(isset($_REQUEST["datepicker"]) && strlen($_REQUEST["datepicker"]) > 0)
	{
		$urlPattern = 'list_bill.php?page=(:num)&datepicker='.$_REQUEST['datepicker'].'';
	}
	
	$paginator = new Paginator($totalItems, $itemsPerPage, $currentPage, $urlPattern);
	
	
	$sel_data_qry = "SELECT * FROM 	bill_master $myCondition";
	if ($ins_qry_data_res = mysql_query($sel_data_qry))
	{ 	// echo "New record created successfully";
		$num_rows = mysql_num_rows($ins_qry_data_res);
		
	}else{  echo "Error: " . $sel_qry . "<br>" . mysql_error($db);  }

?>

<script language="javascript">
function sort(a,b)
{
	document.frm1.user_id.value="";
	document.frm1.sorton.value=a;
	document.frm1.sort.value=b;
	document.frm1.action="lists-athleates.php";
	document.frm1.submit();
}

function setAction(action,billid)
{
	if(action=="delete")
	{
		document.frm1.user_id.value="";
		document.frm1.myaction.value=action;
		
		document.frm1.action="";
		
		mycount = 0;
		for(i=0;i<document.frm1.elements.length;i++)
		{
			if(document.frm1.elements[i].name=="Rec_Ids[]" && document.frm1.elements[i].checked)
				mycount++;
		}
		if(mycount==0)
		{
			alert("You must check one of the checkboxes.");
			return;
		}
		
		if(!confirm("Are you sure you want to "+action+" selected users ?"))
			return;
	
		document.frm1.submit();
	}
	else if(action=="delete_one")
	{
		if(!confirm('Are you sure you want to delete this bill ?')) {
			return;
		}
		
		document.frm1.action="bill_db.php";
		document.frm1.myaction.value=action;
		document.frm1.billid.value=billid;	
		document.frm1.submit();
	}
	else if(action=="edit")
	{
		
	}
	else if(action=="add")
	{
	}
	
	return false;
}
</script>
  

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?=$filename?>
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?=$filename?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">


		 
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              	  	<h3 class="box-title">Show Bills</h3>
             	    <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
			
			
			  
			  
          <!-- /.box-body -->
            <div class="box-footer">
              	
              <form role="form" class="form-horizontal"  action="list_bill.php" name="frm_additem" id="frm_additem" method="post" >
              <input type="hidden" name="myaction" id="myaction"  value="additem" />
              <input type="hidden" name="itemid" id="itemid"  value="" />
              <div class="form-group">
                <div class="col-sm-3" align="right">
                  <label class="control-label">Date:</label>
                </div>
                <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input required type="text" id="datepicker" name="datepicker" value="<?php echo $_REQUEST['datepicker']; ?>" class="form-control pull-right">
                  </div>
                </div>
                <div class="col-sm-3">
                  <button class="btn btn-primary" type="submit">Show Bill</button>
                </div>
                <!-- /.input group -->
              </div>
            </form>	
                
			<!--DATA_TABLE_ST-->
					<div class="row">
					
						  <!--MSG_ST-->
						<?php
						if(isset($_REQUEST['err']) && $_REQUEST['err'] == "del"){
							$msg = "Bill deleted successfully";
						}else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "ins"){
							$msg = "new bill added successfully";
						}else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "none"){
							$msg = "Details updated successfully";
						}else if(isset($_REQUEST['err']) && $_REQUEST['err'] == "success"){
							$msg = "Bill updated successfully";
						}else if(isset($_REQUEST['msg']) && $_REQUEST['msg'] == "success"){
							$msg = "Details updated successfully";
						}
						?>
						
						<?php 
						if( $msg != ""  && ( $_REQUEST['err'] == "none"  ||  $_REQUEST['err'] == "success"  ||  $_REQUEST['err'] == "del"  ||  $_REQUEST['msg'] == "success"  ||  $_REQUEST['err'] == "ins" ) ){ 
						?> 
						<?php /*?>
                        	<div class="col-md-12">
						<div class="alert alert-success alert-dismissible">
							<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
							<h4><i class="icon fa fa-check"></i> <?php echo $msg ; ?>!</h4>
						  </div>
						</div>		
						<?php */?>
                        <script type="text/javascript">
						$( document ).ready(function() {
  							swal("<?php echo $msg; ?>", "", "success")
						 });			
						</script>
						<?php
						} ?>
					  <!--MSG_EN-->
					  
			  
						<div class="col-md-12">
						<!-- BEGIN EXAMPLE TABLE PORTLET-->
						<div class="portlet light bordered">
						<div class="portlet-title">
						<div class="caption font-dark"> <span class="caption-subject bold uppercase">&nbsp;</span> </div>
						</div>
						<div class="portlet-body">
						<div id="sample_1_wrapper" class="dataTables_wrapper no-footer">
						
						<div class="table-scrollable">
						 <form action="" method="post" name="frm1" id="frm1">	
						 
						<table id="sample_1" class="table table-striped table-bordered table-hover table-checkable order-column dataTable no-footer" role="grid" aria-describedby="sample_1_info">
						<thead>
						  <tr role="row" align="center" class="text_center">
							<th rowspan="1" colspan="1" style="width: 68px;" aria-label="">
							</th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Time</th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Customer name</th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Table Number  </th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Status </th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Item Quantity </th>
                            <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Bill Total </th>

                            <th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" >
                                 <input type="checkbox" name="checkAll" id="checkAll"   />
    	                       	 &nbsp;
                                 Add to<br/>template
                            </th>
                            
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> 
								Print 
							</th>																																				
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> 
								Edit 
							</th>
							<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  align="center"> 
								Delete 
							</th>	
							
						  </tr>
						</thead>
						<tbody>
						<?php
							
							
							if($num_rows > 0){
							    
								$total_bils = array();
								
								while($info = mysql_fetch_assoc($ins_qry_data_res)){
								$cnt++;							
									/*
										echo '<pre>';
										print_r($info);
										echo '</pre>';						
									*/
									$hometaken = "0";
									if($info['bill_status'] == "billtaken"){
										$hometaken = "1";	
									}
								
						?>                     
						  <tr <?php if($hometaken == "1"){ echo 'style="background-color:#D2D6DE;"';}?>   class="gradeX odd" role="row">
							<td>
                            	<?php echo $info['billid']; ?>
								<?php //echo $cnt; //$info['aid']; ?>
								<?php //echo '<br/>'. create_order_id($info['bill_date'],$info['bill_time'],$info['billid']);  ?>
								<?php //echo '<br/>'. date('dm', strtotime($info['bill_date']));  ?>
							</td>
							<td class="text_center">
								<?php 	echo $mysqldate =  date('dS M Y', strtotime($info['bill_date']));    ?>
							</td>
							<td class="text_center">
								<?php echo $info['bill_name']; ?>
							</td>
							 <td class="text_center">
								<?php echo $info['bill_table']; ?>
							</td>
							
                            <td align="center">
								<?php 
									echo $info['bill_status']; 
								?>							
							</td>
                            <td align="center">
								<?php 
									 echo $total_bill =  get_bill_item_quantity($info['billid']);
								?>							
							</td>
							<td align="right">
								<?php 
								    $total_bils[] = $info['billid'];
								    echo $total_bill =  get_bill_total($info['billid']);
								?>							
							</td>
                            <td align="center">
	                            <input type="checkbox" class="case" id="addtotemplate" name="addtotemplate[]" value="<?php echo $info['billid']; ?>" />
	                            <input type="hidden" name="addtotemplate_billtotal[<?php echo $info['billid']; ?>]" value="<?php echo $total_bill; ?>" />
                            </td>    
							<td align="center">
								<a class="vierOrder btn-default  label label-sm label-success" data-url="<?php echo $ROOT_SITE_URL ?>printName.php?orderId=<?php echo $info['billid']; ?>">
									<span > <strong> Print With Name </strong></span>
								</a>
							</td>
							<td align="center">
									<a href="edit_bill.php?b=<?php echo $info['billid']; ?>" style="text-decoration:none;">
										<span class="label label-sm label-success"> <strong>Edit </strong></span>
									</a>	
							</td>
							<td align="center">
									<a   href="javascript:setAction('delete_one','<?php echo $info['billid']; ?>');"  style="text-decoration:none;">
										<span class="label label-sm label-success"> <strong>Delete</strong></span>
									</a>	
							</td>
						  </tr>
						<?php
							}
							
							?>
								<tr role="row" align="center" class="text_center">
									<td colspan="6"></td>
									<td colspan="1" align="right">Total  &nbsp;<strong><?php echo $total_bills_amount = total_bills($total_bils);?></strong></td>
									<td colspan="1">
                      					<button id="add_to_template" type="button" class="btn btn-primary">Add to template</button>
				                    </td>
									<td colspan="2"></td>
							  </tr>
							<?php
							
							
						}else{
								echo '<tr role="row" align="center" class="text_center">';
								echo '<td colspan="8">';
								echo 'No Records Found!';
								echo '</td>';
								echo '</tr>';
						}
					?>
						</tbody>
						</table>
							<input type="hidden" name="myaction">
							<input name="billid" type="hidden" id="billid">
							<input type="hidden" name="sorton" value="<?=$sorton?>">
							<input type="hidden" name="sort" value="<?=$sort?>">
						</form>
						</div>
						
						<!--<div align="center">    
							<?php 
							  echo $paginator; 
							?>
						</div>-->
						
						</div>
						</div>
						</div>
						<!-- END EXAMPLE TABLE PORTLET-->
						</div>
					</div>
			<!--DATA_TABLE_EN-->


            </div><!-- /.box-footer-->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div>
	  <!-- /.content-wrapper -->

 <?php
	include_once("includes/footer.php");
?>
<script>
	$( document ).ready(function() {
  	
		$('#datepicker').datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy',
			todayHighlight: true
		});
		
		
		
		// ******** SELECT ALL ST
		  $("#checkAll").change(function () {
				 $("input:checkbox").prop('checked', $(this).prop("checked"));
			});
		// ******** SELECT ALL EN
		
		
		  $("#add_to_template").click(function() {
		  
			  // Ajax update total  FRM 1 ST ----------------------------------
				$.ajax({
					dataType: 'json',
					type: 'POST',
					url: 'ajax_add_to_template.php',
					data: $('#frm1').serialize(),
						success:function(data) 
						{
							 swal("Added To Template successfuly", "", "success");
							 $('input:checkbox').removeAttr('checked');
								
							 console.log(data);	
							 $('#frm_two_tot').html(data['frm_two_total']);
						}
				
				});
			  // Ajax update total  FRM 1 EN ----------------------------------		  
			  
		  });
		
		
	});
</script>
				
				
<script>
	jQuery(document).ready(function() {
		
		$(".vierOrder").click(function(){
			window.open( $(this).attr('data-url'), "Shiv view order","status = 1, height = 800, width = 700, resizable = 0" );
			
		})
		
    });
</script>
