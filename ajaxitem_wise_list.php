<?php 
$check_session = 'no';
include_once("includes/connection_main.php");
$startdate = $_POST['fromdate'];
$enddate = $_POST['todate'];


$syear = $_POST['fyear'];
$smonth = $_POST['fmonth'];

?>
<thead>
	<tr role="row" align="center" class="text_center">
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Item id </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Item Name </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Quantity </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Price (per Item) </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Item Amount </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Weight </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Price (per kg) </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Total Weight Amount </th>
		<th class="text_center" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" > Grand Total </th>
	</tr>
	<?php 
		$selqryweight = "SELECT weight FROM `bill_item_master` group by weight";
		$selectWeightRes = mysql_query($selqryweight) or die(mysql_error());
		$total_qty = 0;	
		$total_amt = 0;
		$total_weight_amt = 0;
		$total_item_amt = 0;
		while ($selectWeightRow = mysql_fetch_array($selectWeightRes)) {
			if ($selectWeightRow['weight'] == '0') {
				if($syear != "")
				{
					$fil_cond ="";
					if($smonth!="all"){
						$fil_cond = " AND month( bm.bill_date ) = '".$smonth."'";
					}
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND year( bm.bill_date ) = '".$syear."' ".$fil_cond." GROUP BY bim.bim_itemid";
				}
				else
				{
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				}
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_item_quantity_price']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_amount']; ?></td>
							<td align="right"><?php echo $selectWeightRow['weight']; ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] == '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['kgPrice']; } ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] == '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['total_amount']; } ?></td>
							<td align="right"><?php  echo $joinItemOrderedItemsRow['total_amount']; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_amount'];
					$total_item_amt = $total_item_amt+$joinItemOrderedItemsRow['total_amount'];
					$total_weight_amt = $total_weight_amt+0;
				}
			}
			elseif($selectWeightRow['weight'] == '250')
			{
			
				if($syear != "")
				{
					$fil_cond ="";
					if($smonth!="all"){
						$fil_cond = " AND month( bm.bill_date ) = '".$smonth."'";
					}
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND year( bm.bill_date ) = '".$syear."' ".$fil_cond." GROUP BY bim.bim_itemid";
				}
				else
				{
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				}
			
			
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['bim_item_quantity_price']; } ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['total_amount']; } ?></td>
							<td align="right">250 Gm</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['kgPrice']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_weight_amt = $total_weight_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_item_amt = $total_item_amt+0;
				}	
			}
			elseif($selectWeightRow['weight'] == '500')
			{
			
				if($syear != "")
				{
					$fil_cond ="";
					if($smonth!="all"){
						$fil_cond = " AND month( bm.bill_date ) = '".$smonth."'";
					}
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND year( bm.bill_date ) = '".$syear."' ".$fil_cond." GROUP BY bim.bim_itemid";
				}
				else
				{
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				}
			
			
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['bim_item_quantity_price']; } ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['total_amount']; } ?></td>
							<td align="right">500 Gm</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['kgPrice']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_weight_amt = $total_weight_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_item_amt = $total_item_amt+0;
				}
			}
			elseif($selectWeightRow['weight'] == '750')
			{
			
				if($syear != "")
				{
					$fil_cond ="";
					if($smonth!="all"){
						$fil_cond = " AND month( bm.bill_date ) = '".$smonth."'";
					}
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND year( bm.bill_date ) = '".$syear."' ".$fil_cond." GROUP BY bim.bim_itemid";
				}
				else
				{
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				}
				
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['bim_item_quantity_price']; } ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['total_amount']; } ?></td>
							<td align="right">750 Gm</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['kgPrice']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_weight_amt = $total_weight_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_item_amt = $total_item_amt+0;
				}
			}
			elseif($selectWeightRow['weight'] == '1000')
			{
			
				if($syear != "")
				{
					$fil_cond ="";
					if($smonth!="all"){
						$fil_cond = " AND month( bm.bill_date ) = '".$smonth."'";
					}
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND year( bm.bill_date ) = '".$syear."' ".$fil_cond." GROUP BY bim.bim_itemid";
				}
				else
				{
					$joinItemOrderedItems = "SELECT bim.billid,bim.bim_itemid,im.kgPrice,bim.bim_item_quantity_price,(sum(bim.bim_item_quantity)*bim_item_quantity_price) as total_amount,(sum(bim.bim_item_quantity_price / 1000)*bim.weight) as total_weight_amount, sum(bim.bim_item_quantity) as total_qty,DATE_FORMAT(bm.bill_date,'%d-%m-%Y') as bill_date,im.itemname FROM bill_item_master AS bim LEFT JOIN bill_master AS bm ON bim.billid=bm.billid LEFT JOIN item_master AS im ON bim.bim_itemid=im.itemid where bim.weight = ".$selectWeightRow['weight']." AND (date_format(bm.bill_date, '%d-%m-%Y') BETWEEN '".$startdate."' AND '".$enddate."') GROUP BY bim.bim_itemid";
				}
				
				$joinItemOrderedItemsRes = mysql_query($joinItemOrderedItems) or die(mysql_error());
				while ($joinItemOrderedItemsRow = mysql_fetch_array($joinItemOrderedItemsRes)) {
						
					?>
						<tr class="gradeX odd" role="row">
							<td align="right"><?php echo $joinItemOrderedItemsRow['bim_itemid']; ?></td>
							<td><?php echo $joinItemOrderedItemsRow['itemname']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_qty']; ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['bim_item_quantity_price']; } ?></td>
							<td align="right"><?php if($selectWeightRow['weight'] != '0'){ echo 0; }else{ echo $joinItemOrderedItemsRow['total_amount']; } ?></td>
							<td align="right">1 Kg</td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['kgPrice']; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
							<td align="right"><?php echo $joinItemOrderedItemsRow['total_weight_amount']+0; ?></td>
						</tr>
					<?php
					$total_qty = $total_qty+$joinItemOrderedItemsRow['total_qty'];
					$total_amt = $total_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_weight_amt = $total_weight_amt+$joinItemOrderedItemsRow['total_weight_amount'];
					$total_item_amt = $total_item_amt+0;
				}
			}
		}
	?>
	<tr class="gradeX odd" role="row">
		<td align="right"></td>
		<td>Total</td>
		<td align="right"><?php echo $total_qty; ?></td>
		<td align="right"></td>
		<td align="right"><?php echo $total_item_amt; ?></td>
		<td align="right"></td>
        <td align="right"></td>
        <td align="right"><?php echo $total_weight_amt; ?></td>
        <td align="right"><?php echo $total_amt; ?></td>
	</tr>     
</thead>
