<?php
	include_once("includes/connection_main.php");

 include_once("includes/header.php");
	include_once("includes/sidebar.php");
/* $username = $DB_Username; 
$password = $DB_Password; 
$hostname = $DB_Host; 
$dbname   = $DB_Name; */

function export_tables($host,$user,$pass,$name,  $tables=false, $backup_name=false )
{
$link = mysqli_connect($host,$user,$pass,$name);
// Check connection
if (mysqli_connect_errno())   {   echo "Failed to connect to MySQL: " . mysqli_connect_error();   }

mysqli_select_db($link,$name);
mysqli_query($link,"SET NAMES 'utf8'");

//get all of the tables
if($tables === false)
{
    $tables = array();
    $result = mysqli_query($link,'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
        $tables[] = $row[0];
    }
}
else
{
    $tables = is_array($tables) ? $tables : explode(',',$tables);
}
$return='';
//cycle through
//print_r($tables);
foreach($tables as $table)
{
    $result = mysqli_query($link,'SELECT * FROM '.$table);
    $num_fields = mysqli_num_fields($result);

    $row2 = mysqli_fetch_row(mysqli_query($link, 'SHOW CREATE TABLE '.$table));
    $return.= "\n\n".$row2[1].";\n\n";

    for ($i = 0; $i < $num_fields; $i++) 
    {
        $st_counter= 0;
        while($row = mysqli_fetch_row($result))
        {
            //create new command if when starts and after 100 command cycle
            if ($st_counter%100 == 0 || $st_counter == 0 )  {
                $return.= "\nINSERT INTO ".$table." VALUES";
            }


            $return.="\n(";
            for($j=0; $j<$num_fields; $j++) 
            {
                $row[$j] = addslashes($row[$j]);
                $row[$j] = str_replace("\n","\\n",$row[$j]);
                if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                if ($j<($num_fields-1)) { $return.= ','; }
            }
            $return.=")";


            //create new command if when starts and after 100 command cycle (but detect this 1 cycle earlier !)
            if ( ($st_counter+1)%100 == 0  && $st_counter != 0 )    {   $return.= ";";  }
            else                                                {   $return.= ",";  }
            //+++++++
            $st_counter = $st_counter +1 ;
        }
        //as we cant detect WHILE loop end, so, just detect, if last command ends with comma(,) then replace it with semicolon(;)
        if (substr($return, -1) == ',') {$return = substr($return, 0, -1). ';'; }
    }
    $return.="\n\n\n";
}
//echo $return;
?>
 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $filename;?>
            <small>Manage your profile details </small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><?php echo $filename;?></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
               
<?php
//save file
if(is_writable("./"))
{
 $backup_name = $backup_name ? $backup_name : $name."___(".date('H-i-s')."_".date('d-m-Y').")__rand".rand(1,11111111).'.sql';
 file_put_contents($backup_name,$return);
 die('SUCCESS. Download BACKUP file: <a target="_blank" href="'.$backup_name.'">'.$backup_name.'</a> <br/><br/>After download, <a target="_blank" href="?delete_filee='.$backup_name.'">Delete it!</a> ');
}
else
{
	echo "Failed to write ";
	
}
 
}

?>
 
</div>
</div>
</section>
<?php
if (!empty($_GET['delete_filee'])){ chdir(dirname(__file__));       
if  (unlink($_GET['delete_filee'])) {die('file_deleted');} 
else                                {die("file doesnt exist");}
}

export_tables($DB_Host,$DB_Username,$DB_Password,$DB_Name);
 
?>